<?php
$aMenu[] = array(
    'parent_menu' => 'global_menu_services',
    'sort' => 10,
    'text' => 'Синхронизация Bitrix24',
    'title' => 'Likee.Sync',
    'icon' => 'util_menu_icon',
    'page_icon' => 'util_page_icon',
    "items_id" => "likee_sync",
    'items' => array(
        array(
            'sort' => 1800,
            'text' => 'Подключенные порталы',
            'title' => 'Likee.Sync',
            'url' => 'likee_sync_admin.php',
            'more_url' => array(
                'likee_sync_admin.php',
                'likee_sync_portal_edit.php'
            ),
            'page_icon' => 'util_page_icon',
        )
    )
);
return $aMenu;