<?
// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

require_once($_SERVER["DOCUMENT_ROOT"] . "/local/modules/likee.sync/include.php"); // инициализация модуля

use Likee\Sync\PortalTable;

// получим права доступа текущего пользователя на модуль
$POST_RIGHT = $APPLICATION->GetGroupRight("likee.sync");
// если нет прав - отправим к форме авторизации с сообщением об ошибке
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm('Доступ запрещен');
?>
<?
// здесь будет вся серверная обработка и подготовка данных
$sTableID = "likee_sync_portal"; // ID таблицы
$oSort = new CAdminSorting($sTableID, "ID", "desc"); // объект сортировки
$lAdmin = new CAdminList($sTableID, $oSort); // основной объект списка

// сохранение отредактированных элементов
if ($lAdmin->EditAction() && $POST_RIGHT == "W") {

    $arModifiedFields = $request->get('FIELDS');
    foreach ($arModifiedFields as $iID => $arFields) {
        $r = PortalTable::update($iID, $arFields);
        if (!$r->isSuccess()) {
            $lAdmin->AddGroupError('Ошибка сохранения' . " " . $r->getErrorMessages(), $ID);
        }
    }
}

// обработка одиночных и групповых действий
if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT == "W") {
    // если выбрано "Для всех элементов"
    if ($_REQUEST['action_target'] == 'selected') {

        $rsData = PortalTable::GetList(array(
            'order' => array($by => $order)
        ));

        while ($arRes = $rsData->Fetch())
            $arID[] = $arRes['ID'];
    }

    // пройдем по списку элементов
    foreach ($arID as $ID) {
        if (strlen($ID) <= 0)
            continue;

        $ID = intval($ID);

        // для каждого элемента совершим требуемое действие
        switch ($_REQUEST['action']) {
            // удаление
            case 'delete':
                $r = PortalTable::delete($ID);
                if (!$r->isSuccess()) {
                    $lAdmin->AddGroupError('Не удалось удалить', $ID);
                }
                break;

            // активация/деактивация
            case 'activate':
            case 'deactivate':
                $arFields = Array("ACTIVE" => ($_REQUEST['action'] == "activate" ? "Y" : "N"));
                $r = PortalTable::update($ID, $arFields);
                if (!$r->isSuccess())
                    $lAdmin->AddGroupError('Ошибка обновления' . $r->getErrorMessages(), $ID);
                break;
        }

    }
}

// выберем список рассылок

$rsPortal = PortalTable::GetList();

// преобразуем список в экземпляр класса CAdminResult
$rsData = new CAdminResult($rsPortal, $sTableID);

// аналогично CDBResult инициализируем постраничную навигацию.
$rsData->NavStart();

// отправим вывод переключателя страниц в основной объект $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("rub_nav")));

$lAdmin->AddHeaders(array(
    array(
        'id' => 'ID',
        'content' => 'ID',
        'sort' => 'ID',
        'default' => true,
    ),
    array(
        'id' => 'ACTIVE',
        'content' => 'Активный',
        'sort' => 'ACTIVE',
        'default' => true,
    ),
    array(
        'id' => 'NAME',
        'content' => 'Название портала',
        'sort' => 'NAME',
        'default' => true,
    ),
    array(
        'id' => 'PORTAL',
        'content' => 'Адрес портала',
        'sort' => 'PORTAL',
        'default' => true,
    ),
    array('id' => 'CLIENT_ID',
        'content' => 'Код приложения',
        'sort' => 'CLIENT_ID',
        'align' => 'right',
        'default' => true,
    ),
    array('id' => 'CLIENT_SECRET',
        'content' => 'Ключ приложения',
        'sort' => 'CLIENT_SECRET',
        'default' => true,
    ),
    array('id' => 'TOKEN',
        'content' => 'Токен',
        'sort' => 'TOKEN',
        'default' => true,
    ),
    array('id' => 'REFRESH_TOKEN',
        'content' => 'Токен обновления',
        'sort' => 'REFRESH_TOKEN',
        'default' => true,
    ),

    array('id' => 'TOKEN_EXPIRE_DATE',
        'content' => 'Токен истекает',
        'sort' => 'TOKEN_EXPIRE_DATE',
        'default' => true,
    ),
    array('id' => 'IS_CLOUD',
        'content' => 'Облачный',
        'sort' => 'ISCLOUD',
        'default' => true,
    ),
    array('id' => 'DEFAULT_EMAIL',
        'content' => 'Логин по умолчанию',
        'sort' => 'DEFAULT_EMAIL',
        'default' => true,
    ),
    array(
        'id' => 'ONLY_NOTIFICATION',
        'content' => 'Режим уведомлений',
        'sort' => 'ONLY_NOTIFICATION',
        'default' => true,
    ),
));

while ($arRes = $rsData->NavNext(true, "f_")) {

    // создаем строку. результат - экземпляр класса CAdminListRow
    $row =& $lAdmin->AddRow($f_ID, $arRes);

    // далее настроим отображение значений при просмотре и редактировании списка

    // параметр NAME будет редактироваться как текст, а отображаться ссылкой
    $row->AddInputField("NAME", array("size" => 25));
    $row->AddViewField("NAME", '<a href="likee_sync_portal_edit.php?ID=' . $f_ID . '">' . $f_NAME . '</a>');

    $row->AddInputField("PORTAL", array("size" => 30));
    $row->AddViewField("PORTAL", '<a target="_blank" href="' . $f_PORTAL . '">' . $f_PORTAL . '</a>');

    $row->AddInputField("CLIENT_ID", array("size" => 30));
    $row->AddInputField("CLIENT_SECRET", array("size" => 60));
    $row->AddInputField("TOKEN", array("size" => 35));
    // параметр SORT будет редактироваться текстом
    $row->AddInputField('SORT', array('size' => 20));

    // флаги ACTIVE и VISIBLE будут редактироваться чекбоксами
    $row->AddCheckField('ACTIVE');

    // использовать только уведомления редактируются чекбоксами
    $row->AddCheckField('ONLY_NOTIFICATION');


    // сформируем контекстное меню
    $arActions = Array();

    // редактирование элемента
    $arActions[] = array(
        "ICON" => "edit",
        "DEFAULT" => true,
        "TEXT" => 'Изменить',
        "ACTION" => $lAdmin->ActionRedirect("likee_sync_portal_edit.php?ID=" . $f_ID)
    );

    // удаление элемента
    if ($POST_RIGHT >= "W")
        $arActions[] = array(
            "ICON" => "delete",
            "TEXT" => 'Удалить',
            "ACTION" => "if(confirm('Удалить?')) " . $lAdmin->ActionDoGroup($f_ID, "delete")
        );


    // применим контекстное меню к строке
    $row->AddActions($arActions);
}
// резюме таблицы
$lAdmin->AddFooter(
    array(
        array("title" => 'Всего: ', "value" => $rsData->SelectedRowsCount()), // кол-во элементов
        array("counter" => true, "title" => 'Выбрано: ', "value" => "0"), // счетчик выбранных элементов
    )
);

// групповые действия
$lAdmin->AddGroupActionTable(Array(
    "delete" => 'Удалить элементы', // удалить выбранные элементы
    "activate" => 'Активировать элеменnы', // активировать выбранные элементы
    "deactivate" => 'Деактивировать элементы', // деактивировать выбранные элементы
));


$aContext = array(
    array(
        "TEXT" => 'Добавить портал',
        "LINK" => "likee_sync_portal_edit.php",
        "TITLE" => 'Добавить портал',
        "ICON" => "btn_new"
    ),
);
$lAdmin->AddAdminContextMenu($aContext);


// альтернативный вывод
$lAdmin->CheckListMode();

// установим заголовок страницы
$APPLICATION->SetTitle('Likee синхронизация');
?>

<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php"); // второй общий пролог
?>

<?
// здесь будет вывод страницы
// выведем таблицу списка элементов
$lAdmin->DisplayList();
?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>