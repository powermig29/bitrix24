<?php

if (empty($_SERVER['DOCUMENT_ROOT']))
    $_SERVER['DOCUMENT_ROOT'] = '/home/bitrix/www';

use Likee\Sync\Task\File;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');


\Bitrix\Main\Loader::includeModule('likee.sync');


$iTaskId = 64271;
$iPortalId = 2;

\Likee\Sync\PortalManager::setFilterActive(false);
$obPortalCloud = \Likee\Sync\PortalManager::getById($iPortalId);
$obPortalBox = \Likee\Sync\PortalManager::getBox();

if (!$obPortalBox || !$obPortalCloud) {
    exit('Portal not found');
}

$arRequest = $obPortalCloud->getTransport()->call(
    'task.item.list',
    array(
        'ORDER' => array('CHANGED_DATE' => 'ASC'),
        'FILTER' => array('ID' => $iTaskId)
    )
);
$arTask = reset($arRequest['result']);

if (!$arTask) {
    exit('Task not found');
}

$arTask['PORTAL_ID'] = $obPortalCloud->getId();
$arTask['PORTAL_ADDRESS'] = $obPortalCloud->getField('PORTAL');

$arCreatedByUser = $obPortalCloud->getUserById($arTask['CREATED_BY']);
$arTask['CREATED_BY_EMAIL'] = $arCreatedByUser['EMAIL'];

$arTask['ACCOMPLICES_EMAILS'] = $arTask['AUDITORS_EMAILS'] = array();

foreach ($arTask['AUDITORS'] as $iAuditorId) {
    if ($arUser = $obPortalCloud->getUserById($iAuditorId))
        $arTask['AUDITORS_EMAILS'][] = $arUser['EMAIL'];
}

foreach ($arTask['ACCOMPLICES'] as $iAuditorId) {
    if ($arUser = $obPortalCloud->getUserById($iAuditorId))
        $arTask['ACCOMPLICES_EMAILS'][] = $arUser['EMAIL'];
}

$arTask['AUDITORS'] = $arTask['ACCOMPLICES'] = array();

$arTask['FILES'] = File::getTaskFiles($obPortalCloud, $arTask['ID']);

/* UPDATE */
$arPortals = \Likee\Sync\PortalManager::getAll();

foreach ($arPortals as $obPortal) {
    if ($obPortal->getId() == $obPortalCloud->getId())
        continue;

    $iBoxTaskId = $obPortal->getBoxTaskId($arTask);

    echo 'box task id - ' . print_r($iBoxTaskId, true) . PHP_EOL;
    echo 'Portal - ' . print_r($obPortal->getField('PORTAL'), true) . PHP_EOL;

    $notice = $obPortal->getField('ONLY_NOTIFICATION');

    if ($notice == 'Y') {
        $obPortal->addNotification($arTask);
    } else {
        if ($obPortal->taskExist($arTask, $iBoxTaskId)) {
            $b = $obPortal->updateTask($arTask, $iBoxTaskId);
        } else {
            $b = $obPortal->addTask($arTask, $iBoxTaskId);
        }
    }

    echo 'result - ' . print_r($b, true) . PHP_EOL;
}