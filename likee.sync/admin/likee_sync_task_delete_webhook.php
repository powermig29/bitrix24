<?php

use Bitrix\Main\Config\Option;

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

if (Option::get('likee.synk', 'use_webhook', 'Y') !== 'Y')
    return;

\Bitrix\Main\Loader::includeModule('likee.sync');

$iTaskId = intval($_REQUEST['data']['FIELDS_BEFORE']['ID']);
$sDomain = htmlspecialchars(trim($_REQUEST['auth']['domain']));
$sEvent = htmlspecialchars(trim($_REQUEST['event']));

file_put_contents(
    $_SERVER['DOCUMENT_ROOT'] . '/synclogs/webhook.log',
    'START - ' . date('d.m.Y H:i:s') . ' - ' . print_r($_REQUEST, true) . PHP_EOL,
    FILE_APPEND
);

if ($iTaskId <= 0 || !$sDomain || !in_array($sEvent, ['ONTASKDELETE']))
    return;

$obCurPortal = \Likee\Sync\PortalManager::getByDomain($sDomain);

if (!$obCurPortal)
    return;

$sWebHookAuthToken = $obCurPortal->getField('WEB_HOOK_TOKEN');

if ($sWebHookAuthToken !== $_REQUEST['auth']['application_token']) {
    file_put_contents(
        $_SERVER['DOCUMENT_ROOT'] . '/synclogs/webhook.log',
        date('d.m.Y H:i:s') . ' ' . $_REQUEST['auth']['application_token'] . ' INVALID APPLICATION TOKEN' . PHP_EOL,
        FILE_APPEND
    );
    return;
}

$arTask = ['PORTAL_ID' => $obCurPortal->getId(), 'ID' => $iTaskId];

file_put_contents(
    $_SERVER['DOCUMENT_ROOT'] . '/synclogs/webhook.log',
    date('d.m.Y H:i:s') . ' TASK - ' . print_r($arTask, true) . PHP_EOL,
    FILE_APPEND
);

$iBoxTaskId = $obCurPortal->getBoxTaskId($arTask);

if ($iBoxTaskId <= 0)
    return;

file_put_contents(
    $_SERVER['DOCUMENT_ROOT'] . '/synclogs/webhook.log',
    date('d.m.Y H:i:s') . ' BOX_TASK_ID - ' . $iBoxTaskId . PHP_EOL,
    FILE_APPEND
);

$rsTasks = \Likee\Sync\TaskTable::getList(array(
    'filter' => array('TASK_ID' => $iBoxTaskId)
));

$arTasks = array();

while ($arTask = $rsTasks->fetch()) {

    try {
        if ($arTask['PORTAL_ID'] != $obCurPortal->getId()) {
            $obPortal = \Likee\Sync\PortalManager::getById($arTask['PORTAL_ID']);
            if ($obPortal) {
                $obPortal->deleteTask($arTask['PORTAL_TASK_ID']);
            }
        }
    } catch (\Exception $exception) {
    }

    \Likee\Sync\TaskTable::delete($arTask['ID']);
}

$obPortalBox = \Likee\Sync\PortalManager::getBox();
if ($obPortalBox) {
    try {
        $obPortalBox->deleteTask($iBoxTaskId);
    } catch (\Exception $exception) {
    }
}

file_put_contents(
    $_SERVER['DOCUMENT_ROOT'] . '/synclogs/webhook.log',
    date('d.m.Y H:i:s') . ' - END' . PHP_EOL,
    FILE_APPEND
);