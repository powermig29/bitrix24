<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

\Bitrix\Main\Loader::includeModule('likee.sync');

// тут file_id это object_id, id из таблицы b_disk_object
$iFileId = intval($_REQUEST['file_id']);

if ($iFileId <= 0)
    return;

$file = \Bitrix\Disk\File::loadById($iFileId, array('STORAGE'));
$arFile = $file->getFile();

if (!$arFile)
    return;

\CFile::viewByUser($arFile, array('force_download' => false, 'cache_time' => 0, 'attachment_name' => $file->getName()));