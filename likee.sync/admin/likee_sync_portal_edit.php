<?
/**
 * @global $DB
 * @global $APPLICATION
 *
 * @var $REQUEST_METHOD
 * @var $apply
 * @var $save
 */
// подключим все необходимые файлы:
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php'); // первый общий пролог

require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/likee.sync/include.php'); // инициализация модуля

\Bitrix\Main\Loader::includeModule('socialservices');

use Bitrix\Main\Type\DateTime;
use \Likee\Sync\PortalTable;
use \Likee\Sync\Portal;

CJSCore::Init(array('jquery'));

// получим права доступа текущего пользователя на модуль
$POST_RIGHT = $APPLICATION->GetGroupRight('likee.sync');
// если нет прав - отправим к форме авторизации с сообщением об ошибке
if ($POST_RIGHT == 'D')
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

// сформируем список закладок
$aTabs = array(
    array('DIV' => 'edit1', 'TAB' => 'Портал', 'ICON' => 'main_user_edit', 'TITLE' => 'Редактирование портала'),
);
$tabControl = new CAdminTabControl('tabControl', $aTabs);

$ID = intval($request->get('ID'));  // идентификатор редактируемой записи

if ($ID > 0)
    $obPortal = new Portal($ID);

$message = null;        // сообщение об ошибке
$bVarsFromForm = false; // флаг "Данные получены с формы", обозначающий, что выводимые данные получены с формы, а не из БД.

// ******************************************************************** //
//                ОБРАБОТКА ИЗМЕНЕНИЙ ФОРМЫ                             //
// ******************************************************************** //

if (
    $REQUEST_METHOD == 'POST' // проверка метода вызова страницы
    &&
    (!empty($save) || !empty($apply)) // проверка нажатия кнопок 'Сохранить' и 'Применить'
    &&
    $POST_RIGHT == 'W'          // проверка наличия прав на запись для модуля
    &&
    check_bitrix_sessid()     // проверка идентификатора сессии
) {
    $bIsCloud = !empty($obPortal) && $obPortal->isCloud();

    $arPostFields = $request->toArray();

    // обработка данных формы
    $arFields = array(
        'ACTIVE' => $arPostFields['ACTIVE'] != 'Y' ? 'N' : 'Y',
        'NAME' => $arPostFields['NAME'],
        'PORTAL' => htmlspecialchars($arPostFields['PORTAL']),
        'CLIENT_ID' => $arPostFields['CLIENT_ID'],
        'CLIENT_SECRET' => $arPostFields['CLIENT_SECRET'],
        'TOKEN' => $arPostFields['TOKEN'],
        'REFRESH_TOKEN' => $arPostFields['REFRESH_TOKEN'],
        'WEB_HOOK_TOKEN' => $arPostFields['WEB_HOOK_TOKEN'],
        'TOKEN_EXPIRE_DATE' => empty($arPostFields['TOKEN_EXPIRE_DATE']) ? false : new DateTime($arPostFields['TOKEN_EXPIRE_DATE']),
        'DEFAULT_EMAIL' => $arPostFields['DEFAULT_EMAIL'],
        'ONLY_NOTIFICATION' => $arPostFields['ONLY_NOTIFICATION'] != 'Y' ? 'N' : 'Y',
    );

    $arRequiredFields = array(
        'NAME' => 'Название'
    );

    if ($bIsCloud) {
        $arRequiredFields = array_merge(
            $arRequiredFields,
            array(
                'PORTAL' => 'Портал',
                'CLIENT_ID' => 'Код приложения',
                'CLIENT_SECRET' => 'Ключ приложения'
            )
        );
    }

    foreach ($arRequiredFields as $sFiled => $sName) {
        if (empty($arFields[$sFiled])) {
            $message = new CAdminMessage('Поле "' . $sName . '" обязательнро для заполнения');
            break;
        }
    }

    if (empty($message) && !filter_var($arFields['PORTAL'], FILTER_VALIDATE_URL) && $bIsCloud)
        $message = new CAdminMessage('Поле "Портал" должно содержать ссылку на портал');

    if (empty($message)) {
        // сохранение данных
        if ($ID > 0) {
            $result = PortalTable::Update($ID, $arFields);
        } else {
            $result = PortalTable::Add($arFields);
        }

        if ($result->isSuccess()) {
            // если сохранение прошло удачно - перенаправим на новую страницу
            // (в целях защиты от повторной отправки формы нажатием кнопки "Обновить" в браузере)
            if (!empty($apply))
                // если была нажата кнопка "Применить" - отправляем обратно на форму.
                LocalRedirect('/bitrix/admin/likee_sync_portal_edit.php?ID=' . $result->getId() . '&mess=ok&' . $tabControl->ActiveTabParam());
            else
                // если была нажата кнопка "Сохранить" - отправляем к списку элементов.
                LocalRedirect('/bitrix/admin/likee_sync_admin.php');
        } else {
            $message = new CAdminMessage('Ошибка сохранения! ' . print_r($result->getErrorMessages()));
        }
    }
}

if (!empty($request->get('state'))) {
    $obPortal = new Portal($request->get('state'));
    $arTokenData = $obPortal->getToken($request->get('code'));
    ?>
    <script>
        <? if (is_array($arTokenData) && (empty($arTokenData['error']))): ?>
        window.opener.document.getElementsByName('TOKEN')[0].value = '<?= $arTokenData['access_token']; ?>';
        window.opener.document.getElementsByName('REFRESH_TOKEN')[0].value = '<?= $arTokenData['refresh_token']; ?>';
        window.opener.document.getElementsByName('TOKEN_EXPIRE_DATE')[0].value = '<?= (new DateTime())->add(intval($arTokenData['expires_in']) . ' seconds'); ?>';
        window.opener.document.getElementsByName('TOKEN_IS_VALID')[0].innerHTML = '<td></td><td>Токен получен</td>';
        var adminMessage = window.opener.document.getElementsByClassName('adm-info-message');
        if (adminMessage.length > 0)
            adminMessage[0].style = 'display:none';
        <? else: ?>
        window.opener.alert('<?= $arTokenData ?>');
        <? endif; ?>
        window.close();
    </script>
    <?
}


// ******************************************************************** //
//                ВЫБОРКА И ПОДГОТОВКА ДАННЫХ ФОРМЫ                     //
// ******************************************************************** //

// значения по умолчанию
$str_SORT = 100;
$str_ACTIVE = 'Y';
$str_AUTO = 'N';
$str_DAYS_OF_MONTH = '';
$str_DAYS_OF_WEEK = '';
$str_TIMES_OF_DAY = '';
$str_VISIBLE = 'Y';
$str_LAST_EXECUTED = ConvertTimeStamp(false, 'FULL');
$str_FROM_FIELD = COption::GetOptionString('subscribe', 'default_from');

// выборка данных
$arPortal = !empty($obPortal) ? $obPortal->getFields() : [];

// если данные переданы из формы, инициализируем их
if ($bVarsFromForm)
    $DB->InitTableVarsForEdit('likee_sync_portal', '', 'str_');

// ******************************************************************** //
//                ВЫВОД ФОРМЫ                                           //
// ******************************************************************** //

// установим заголовок страницы
$APPLICATION->SetTitle(($ID > 0 ? 'Портал ' . $arPortal['NAME'] : 'Добавление портала'));

// не забудем разделить подготовку данных и вывод
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');

$aMenu = array(
    array(
        'TEXT' => 'К списку порталов',
        'LINK' => '/bitrix/admin/likee_sync_admin.php',
        'ICON' => 'btn_list',
        'TITLE' => 'К списку порталов',
    )
);

$context = new CAdminContextMenu($aMenu);
$context->Show();

?>

<?
// если есть сообщения об ошибках или об успешном сохранении - выведем их.
if ($_REQUEST['mess'] == 'ok' && $ID > 0)
    CAdminMessage::ShowMessage(array('MESSAGE' => 'Сохранение успешно', 'TYPE' => 'OK'));

if (!empty($message)) {
    echo $message->Show();
}
?>

<?
// далее выводим собственно форму
?>
    <form method="POST" action="<?= $APPLICATION->GetCurPage() ?>" enctype="multipart/form-data" name="post_form">
        <?= bitrix_sessid_post(); ?>

        <?
        $tabControl->Begin();
        $tabControl->BeginNextTab();
        ?>

        <input type="hidden" name="REFRESH_TOKEN" value="<?= $arPortal['REFRESH_TOKEN'] ?>">
        <?
        /** @var DateTime $obDate */
        $obDate = $arPortal['TOKEN_EXPIRE_DATE'];
        ?>
        <input type="hidden" name="TOKEN_EXPIRE_DATE" value="<?= $obDate; ?>">

        <tr>
            <td width="40%">Активный</td>
            <td width="60%">
                <input type="checkbox" name="ACTIVE" value="Y"<? if ($arPortal['ACTIVE'] == 'Y') echo ' checked'; ?>>
            </td>
        </tr>
        <tr>
            <td width="40%">Название</td>
            <td width="60%"><input type="text" name="NAME" value="<?= htmlspecialchars($arPortal['NAME']); ?>" required>
            </td>
        </tr>
        <tr>
            <td>Портал</td>
            <td>
                <input type="text" name="PORTAL" value="<?= $arPortal['PORTAL'] ?>" required >
            </td>
        </tr>
        <tr>
            <td>Код приложения</td>
            <td>
                <input type="text" name="CLIENT_ID" value="<?= $arPortal['CLIENT_ID'] ?>" <?= $arPortal['IS_CLOUD'] == 'N' ? 'readonly' : 'required' ?>>
            </td>
        </tr>
        <tr>
            <td>Ключ приложения</td>
            <td>
                <input type="text" name="CLIENT_SECRET" value="<?= $arPortal['CLIENT_SECRET'] ?>" <?= $arPortal['IS_CLOUD'] == 'N' ? 'readonly' : 'required' ?>>
            </td>
        </tr>
        <tr>
            <td>Логин по умолчанию</td>
            <td>
                <input type="text" name="DEFAULT_EMAIL" value="<?= $arPortal['DEFAULT_EMAIL'] ? $arPortal['DEFAULT_EMAIL'] : 'default@bx24.ru' ?>" required>
            </td>
        </tr>
        <tr>
            <td width="40%">Режим уведомлений</td>
            <td width="60%">
                <input type="checkbox" name="ONLY_NOTIFICATION" value="Y"<? if ($arPortal['ONLY_NOTIFICATION'] == 'Y') echo ' checked'; ?>>
            </td>
        </tr>
        <tr>
            <td>Код авторизации вебхука</td>
            <td>
                <input type="text" name="WEB_HOOK_TOKEN" value="<?= $arPortal['WEB_HOOK_TOKEN'] ?>">
            </td>
        </tr>

        <? if ($ID > 0 && $arPortal['IS_CLOUD'] == 'Y'): ?>
            <tr>
                <td>Токен приложения</td>
                <td>
                    <input type="text" name="TOKEN" value="<?= $arPortal['TOKEN'] ?>">
                </td>
            </tr>
            <tr name="TOKEN_IS_VALID">
                <? if (empty($arPortal['TOKEN_EXPIRE_DATE'])): ?>
                    <td></td>
                    <td>
                        <a href="#" onclick="getToken(event)" class="adm-btn">Получить токен</a>
                    </td>
                <? elseif (new DateTime($arPortal['TOKEN_EXPIRE_DATE']) < new DateTime()): ?>
                    <td>Неверный токен</td>
                    <td>
                        <a href="#" onclick="getToken(event)" class="adm-btn">Получить токен</a>
                    </td>
                <? else: ?>
                    <td>Токен истекает</td>
                    <td>
                        <?= $arPortal['TOKEN_EXPIRE_DATE'] ?>
                    </td>
                <? endif; ?>
            </tr>
        <? endif; ?>

        <?
        // завершение формы - вывод кнопок сохранения изменений
        $tabControl->Buttons(
            array(
                'disabled' => ($POST_RIGHT < 'W'),
                'back_url' => 'likee_sync_admin.php',
            )
        );
        ?>
        <? if ($ID > 0): ?>
            <input type="hidden" name="ID" value="<?= $ID ?>">
        <? endif; ?>

        <?
        $tabControl->End();
        ?>

        <script>

            function getToken(event) {
                event.preventDefault();
                var errors = {};
                var portal = $('input[name="PORTAL"]').val();
                var client_id = $('input[name="CLIENT_ID"]').val();
                var client_secret = $('input[name="CLIENT_SECRET"]').val();

                var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
                if (!regex.test(portal)) {
                    errors['portal'] = 'not_valid';
                }

                if (portal.length < 1)
                    errors['portal'] = 'empty';
                if (client_id.length < 1)
                    errors['client_id'] = 'empty';
                if (client_secret.length < 1)
                    errors['client_secret'] = 'empty';

                if (!$.isEmptyObject(errors)) {
                    return false;
                }

                var portallocation = portal +
                    '/oauth/authorize/?client_id=' +
                    client_id +
                    '&state= <?= $ID ?>';

                window.open(portallocation, '_blank', 'width=800, height=900');

                return false;
            }
        </script>
<?
// завершение страницы
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
?>