<?php

if (empty($_SERVER['DOCUMENT_ROOT']))
    $_SERVER['DOCUMENT_ROOT'] = '/home/bitrix/www';

use Likee\Sync\Task\File;

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');


\Bitrix\Main\Loader::includeModule('likee.sync');

$iTaskId = 58658;

$obPortalBox = new \Likee\Sync\Portal(1);

if (!$obPortalBox) {
    exit('Portal not found');
}

$rsLocalTask = \CTasks::GetList(
    array('CHANGED_DATE' => 'ASC'),
    array(
        'ID' => $iTaskId,
        'CHECK_PERMISSIONS' => 'N'
    )
);

if (!($arTask = $rsLocalTask->Fetch())) {
    exit('Task not found');
}

$rsMembers = \CTaskMembers::GetList(array(), array('TASK_ID' => $arTask['ID']));
while ($arMember = $rsMembers->Fetch()) {
    if ($arMember['TYPE'] == 'A') {
        $arTask['ACCOMPLICES'][] = $arMember['USER_ID'];
    } elseif ($arMember['TYPE'] == 'U') {
        $arTask['AUDITORS'][] = $arMember['USER_ID'];
    }
}

$arTask['PORTAL_ID'] = $obPortalBox->getId();
$arTask['PORTAL_ADDRESS'] = $obPortalBox->getField('PORTAL');

$arCreatedByUser = $obPortalBox->getUserById($arTask['CREATED_BY']);
$arTask['CREATED_BY_EMAIL'] = $arCreatedByUser['EMAIL'];

$arTask['ACCOMPLICES_EMAILS'] = $arTask['AUDITORS_EMAILS'] = array();

foreach ($arTask['AUDITORS'] as $iAuditorId) {
    if ($arUser = $obPortalBox->getUserById($iAuditorId))
        $arTask['AUDITORS_EMAILS'][] = $arUser['EMAIL'];
}

foreach ($arTask['ACCOMPLICES'] as $iAuditorId) {
    if ($arUser = $obPortalBox->getUserById($iAuditorId))
        $arTask['ACCOMPLICES_EMAILS'][] = $arUser['EMAIL'];
}

$arTask['AUDITORS'] = $arTask['ACCOMPLICES'] = array();

$arTask['FILES'] = File::getTaskFiles($obPortalBox, $arTask['ID']);

/* UPDATE */

\Likee\Sync\PortalManager::setFilterActive(false);
$arPortals = \Likee\Sync\PortalManager::getAll();

foreach ($arPortals as $obPortal) {
    if (!$obPortal->isCloud())
        continue;

    echo 'Portal - ' . print_r($obPortal->getField('PORTAL'), true) . PHP_EOL;

    $notice = $obPortal->getField('ONLY_NOTIFICATION');

    if ($notice == 'Y') {
        $obPortal->addNotification($arTask);
    } else {
        if ($obPortal->taskExist($arTask, $iTaskId)) {
            $b = $obPortal->updateTask($arTask, $iTaskId);
        } else {
            $b = $obPortal->addTask($arTask, $iTaskId);
        }
    }

}