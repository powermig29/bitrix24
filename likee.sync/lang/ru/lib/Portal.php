<?php
/**
 * Created by PhpStorm.
 * User: powermig29
 * Date: 02.10.17
 * Time: 10:30
 */

$MESS['NOTIFY_NEW_MESSAGE'] = 'Для вас была добавлена задача: [b]#TASK_TITLE#[/b] на портале #PORTAL_ADDRESS# от #CREATED_BY_NAME# #CREATED_BY_LAST_NAME#.#BR##BR#';
$MESS['NOTIFY_NEW_MESSAGE_LINK'] = 'Ознакомиться с задачей можно по: [url=#TASK_URL#]этой ссылке[/url]';
$MESS['PORTAL_TASK_URL'] = '#PORTAL_ADDRESS#/company/personal/user/#RESPONSIBLE_ID#/tasks/task/view/#TASK_ID#/';

$MESS['NOTIFY_UPDATE_MESSAGE'] = 'Задача [b]#TASK_TITLE#[/b] на портале #PORTAL_ADDRESS# была обновлена.#BR##BR#';
