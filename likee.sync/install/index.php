<?

class likee_sync extends CModule
{
    const MODULE_ID = 'likee.sync';

    public $MODULE_ID = 'likee.sync',
        $MODULE_VERSION,
        $MODULE_VERSION_DATE,
        $MODULE_NAME = 'Синхронизация Fakel',
        $PARTNER_NAME = 'Likee',
        $PARTNER_URI = 'http://likee.ru',
        $DIR;

    public function __construct()
    {
        $arModuleVersion = array();
        include __DIR__ . 'version.php';

        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

        if (stripos(__FILE__, $_SERVER['DOCUMENT_ROOT'] . '/local/modules') !== false) {
            $this->DIR = 'local';
        } else {
            $this->DIR = 'bitrix';
        }
    }

    function InstallDB()
    {
        global $DB;
        $sSqlQuery = 'CREATE TABLE likee_sync_portal (' .
            'ID int NOT NULL AUTO_INCREMENT,' .
            'NAME varchar(255),' .
            'PORTAL varchar(255),' .
            'CLIENT_ID varchar(255),' .
            'CLIENT_SECRET varchar(255),' .
            'TOKEN varchar(255),' .
            'REFRESH_TOKEN varchar(255),' .
            'SORT integer,' .
            'DATE_ADDED datetime,' .
            'TOKEN_EXPIRE_DATE datetime,' .
            'DEFAULT_EMAIL varchar(255),' .
            'ACTIVE varchar(1),' .
            'IS_CLOUD varchar(1),' .
            'ONLY_NOTIFICATION varchar(1),' .
            'WEB_HOOK_TOKEN varchar(255),' .
            'PRIMARY KEY (ID)' .
            ');';

        if (!$DB->TableExists('likee_sync_portal'))
            $DB->Query($sSqlQuery);

        $DB->Query('INSERT INTO likee_sync_portal (NAME , ACTIVE, IS_CLOUD, DEFAULT_EMAIL) VALUES ("local", "Y", "N", "default@bx24.ru");');

        $sSqlQuery = 'CREATE TABLE likee_sync_task (' .
            'ID int NOT NULL AUTO_INCREMENT,' .
            'TASK_ID int NOT NULL,' .
            'PORTAL_ID int NOT NULL,' .
            'PORTAL_TASK_ID int NOT NULL,' .
            'ORIGIN_PORTAL_ID int NOT NULL,' .
            'PRIMARY KEY (ID));';

        if (!$DB->TableExists('likee_sync_task'))
            $DB->Query($sSqlQuery);

        $sSqlQuery = 'CREATE TABLE likee_sync_notification (' .
            'ID int NOT NULL AUTO_INCREMENT,' .
            'PORTAL_ID int NOT NULL,' .
            'TASK_ID int NOT NULL,' .
            'PRIMARY KEY (ID));';

        if (!$DB->TableExists('likee_sync_notification'))
            $DB->Query($sSqlQuery);

        $sSqlQuery = 'CREATE TABLE likee_sync_checklist (' .
            'ID int NOT NULL AUTO_INCREMENT,' .
            'CHECKLIST_ID int NOT NULL,' .
            'PORTAL_ID int NOT NULL,' .
            'XML_ID int NOT NULL,' .
            'PRIMARY KEY (ID));';

        if (!$DB->TableExists('likee_sync_checklist'))
            $DB->Query($sSqlQuery);

        $sSqlQuery = 'CREATE TABLE likee_sync_file (' .
            'ID int NOT NULL AUTO_INCREMENT,' .
            'FILE_ID int NOT NULL,' .
            'PORTAL_ID int NOT NULL,' .
            'XML_ID int NOT NULL,' .
            'PRIMARY KEY (ID));';

        if (!$DB->TableExists('likee_sync_file'))
            $DB->Query($sSqlQuery);
    }

    function UninstallDB()
    {
        global $DB;

        //$DB->Query('DROP TABLE likee_sync_portal');

        //$DB->Query('DROP TABLE likee_sync_task');
        //$DB->Query('DROP TABLE likee_sync_notification');
        //$DB->Query('DROP TABLE likee_sync_checklist');
        //$DB->Query('DROP TABLE likee_sync_file');
    }

    function InstallAgent()
    {
        \CAgent::AddAgent('Likee\Sync\Agent::syncTasks();', self::MODULE_ID, 'N', 60);
        \CAgent::AddAgent('Likee\Sync\Agent::syncComments();', self::MODULE_ID, 'N', 60);
    }

    function InstallHandlers()
    {
        RegisterModuleDependences('tasks', 'OnTaskDelete', self::MODULE_ID, '\Likee\Sync\Handlers', 'deleteTask');
    }

    function UninstallHandlers()
    {
        UnRegisterModuleDependences('tasks', 'OnTaskDelete', self::MODULE_ID, '\Likee\Sync\Handlers', 'deleteTask');
    }

    function UninstallAgent()
    {
        $rsAgents = \CAgent::GetList(array(), array('MODULE_ID' => self::MODULE_ID));
        while ($arAgent = $rsAgents->Fetch()) {
            \CAgent::Delete($arAgent['ID']);
        }
    }

    public function InstallFiles()
    {
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->DIR . '/modules/' . $this->MODULE_ID . '/admin')) {
            if ($dir = opendir($p)) {
                while (false !== $item = readdir($dir)) {
                    if ($item == '..' || $item == '.' || $item == 'menu.php')
                        continue;

                    file_put_contents(
                        $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $item,
                        '<' . '? require($_SERVER["DOCUMENT_ROOT"]."/' . $this->DIR . '/modules/' . $this->MODULE_ID . '/admin/' . $item . '");?' . '>'
                    );
                }
                closedir($dir);
            }
        }
    }

    public function UninstallFiles()
    {
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->DIR . '/modules/' . $this->MODULE_ID . '/admin')) {
            if ($dir = opendir($p)) {
                while (false !== $item = readdir($dir)) {
                    if ($item == '..' || $item == '.')
                        continue;
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $item);
                }
                closedir($dir);
            }
        }

    }

    public function DoInstall()
    {
        RegisterModule($this->MODULE_ID);

        $this->InstallDB();
        $this->InstallFiles();
        $this->InstallAgent();
        $this->InstallHandlers();


    }

    public function DoUninstall()
    {
        $this->UninstallHandlers();
        UnRegisterModule($this->MODULE_ID);
        $this->UninstallDB();
        $this->UninstallFiles();
        $this->UninstallAgent();

    }
}