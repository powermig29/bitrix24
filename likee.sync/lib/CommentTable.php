<?php

namespace Likee\Sync;

use Bitrix\Main\Entity;

class CommentTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'likee_sync_comment';
    }

    public static function getFile()
    {
        return __FILE__;
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),

            new Entity\TextField('MESSAGE'),

            new Entity\StringField('AUTHOR_EMAIL'),

        );
    }
}