<?php

namespace Likee\Sync;

use Bitrix\Main\Entity;
use Bitrix\Main\Type\DateTime;

class PortalTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'likee_sync_portal';
    }

    public static function getFile()
    {
        return __FILE__;
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),

            new Entity\StringField('NAME'),

            new Entity\StringField('PORTAL'),

            new Entity\StringField('CLIENT_ID'),

            new Entity\StringField('CLIENT_SECRET'),

            new Entity\StringField('TOKEN'),

            new Entity\StringField('REFRESH_TOKEN'),

            new Entity\StringField('WEB_HOOK_TOKEN'),

            new Entity\DatetimeField('TOKEN_EXPIRE_DATE'),

            new Entity\StringField('DEFAULT_EMAIL', array(
                'default_value' => 'default@bx24.ru'
            )),

            new Entity\IntegerField('SORT', array(
                'default_value' => 100
            )),

            new Entity\DatetimeField('DATE_ADDED', array(
                'default_value' => new DateTime()
            )),

            new Entity\BooleanField('ACTIVE', array(
                'values' => array('N', 'Y')
            )),

            new Entity\BooleanField('IS_CLOUD', array(
                'values' => array('N', 'Y'),
                'default_value' => 'Y'
            )),

            new Entity\BooleanField('ONLY_NOTIFICATION', array(
                'values' => array('N', 'Y'),
                'default_value' => 'Y'
            )),
        );
    }
}

