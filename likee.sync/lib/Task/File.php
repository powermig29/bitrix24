<?php
/**
 * User: Azovcev Artem
 * Date: 14.09.17
 * Time: 10:48
 */

namespace Likee\Sync\Task;


use Likee\Sync\FileTable;
use Likee\Sync\Portal;
use Likee\Sync\PortalManager;

class File
{
    /**
     * @param Portal $obPortal
     * @param int $iTaskId
     * @return array
     */
    public static function getTaskFiles(Portal $obPortal, $iTaskId)
    {
        $arFiles = array();

        if ($obPortal->isCloud()) {
            $arResponse = $obPortal->getTransport()->call('task.item.getfiles', array($iTaskId));

            if (empty($arResponse['result']))
                return $arFiles;

            $arFiles = $arResponse['result'];
        } else {
            $arFilesId = \CTaskItem::getInstance($iTaskId, 1)->getAttachmentIds();
            foreach ($arFilesId as $iFileId) {
                $arFiles[] = Attachment::getById($iFileId);
            }
        }

        foreach ($arFiles as &$arFile) {
            $arFile['ID'] = $arFile['FILE_ID'];
            $arFile['PORTAL_ID'] = $obPortal->getId();
        }
        unset($arFile);

        return $arFiles;
    }

    /**
     * @param Portal $obPortal - портал в котоый будет добавлен файл
     * @param int $iTaskId - задачав в которую будет добавлен файл
     * @param array $arFile - массив с данными о файле
     * @param int|null $iBoxFileId
     * @return int - id нового файла на кортале
     * @throws \Exception
     */
    public static function addTaskFile(Portal $obPortal, $iTaskId, $arFile, $iBoxFileId = null)
    {
        $arPortalSrc = PortalManager::getById($arFile['PORTAL_ID']);

        if (empty($arPortalSrc))
            return 0;

        if ($arPortalSrc->isCloud()) {
            $arDiskFile = $arPortalSrc->getTransport()->call('disk.file.get', ['id' => $arFile['FILE_ID']]);

            if (!empty($arDiskFile['error']))
                throw new \Exception($arDiskFile['error_description'] ?: $arDiskFile['error']);

            $sDownloadUrl = $arDiskFile['result']['DOWNLOAD_URL'];
        } else {
            $sDownloadUrl = $arPortalSrc->getField('PORTAL') . '/local/modules/likee.sync/admin/show_file.php';
            $sDownloadUrl .= '?file_id=' . $arFile['FILE_ID'];
        }

        $sContent = file_get_contents($sDownloadUrl);

        if (empty($sContent))
            return 0;

        if ($obPortal->isCloud()) {
            $arResponse = $obPortal->getTransport()->call(
                'task.item.addfile',
                array(
                    'TASK_ID' => $iTaskId,
                    'FILE' => array(
                        'NAME' => $arFile['NAME'],
                        'CONTENT' => base64_encode($sContent)
                    )
                )
            );

            $iFileId = intval($arResponse['result']['FILE_ID']);

            if ($iFileId <= 0)
                throw new \Exception('CLOUD_FILE_NOT_CREATE');

            $b = FileTable::add(array(
                'FILE_ID' => $iBoxFileId,
                'PORTAL_ID' => $obPortal->getId(),
                'XML_ID' => $iFileId
            ));

            if (!$b->isSuccess())
                throw new \Exception('CLOUD_FILE_NOT_CREATE_2');

            return $iFileId;
        } else {
            $iFileId = (int)Attachment::add(
                $iTaskId,
                array(
                    'NAME' => $arFile['NAME'],
                    'CONTENT' => base64_encode($sContent)
                ),
                array(
                    'USER_ID' => 1,
                    'ENTITY_ID' => \Bitrix\Tasks\Integration\Rest\Task\UserField::getTargetEntityId(),
                    'FIELD_NAME' => 'UF_TASK_WEBDAV_FILES'
                )
            );
            \CTaskItem::getInstance($iTaskId, 1)->markCacheAsDirty();

            if ($iFileId <= 0)
                throw new \Exception('BOX_FILE_NOT_CREATE');

            $b = FileTable::add(array(
                'FILE_ID' => $iFileId,
                'PORTAL_ID' => $arFile['PORTAL_ID'],
                'XML_ID' => $arFile['ID']
            ));

            if (!$b->isSuccess())
                throw new \Exception('BOX_FILE_NOT_CREATE_2');

            return 0;
        }
    }
}