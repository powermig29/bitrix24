<?php
/**
 * User: Azovcev Artem
 * Date: 18.09.17
 * Time: 21:06
 */

namespace Likee\Sync\Task;

use Bitrix\Disk\Driver;

use Bitrix\Rest\RestException;
use Bitrix\Rest\AccessException;


class Attachment extends \Bitrix\Tasks\Integration\Disk\Rest\Attachment
{
    protected static function uploadFile($name, $content, array $parameters = array())
    {
        static::includeDisk();

        $storage = Driver::getInstance()->getStorageByUserId($parameters['USER_ID']);
        if (!$storage) {
            throw new RestException("Could not find storage for user '" . $parameters['USER_ID'] . "'.", RestException::ERROR_NOT_FOUND);
        }

        $folder = $storage->getFolderForUploadedFiles();
        if (!$folder) {
            return false;
        }

//        $securityContext = $storage->getCurrentUserSecurityContext();
//        if (!$folder->canAdd($securityContext)) {
//            throw new AccessException;
//        }

        $fileData = \CRestUtil::saveFile($content);
        if (!$fileData) {
            throw new RestException('Could not save file');
        }

        $file = $folder->uploadFile($fileData, array(
            'NAME' => $name,
            'CREATED_BY' => $parameters['USER_ID']
        ), array(), true);

        if (!$file) {
            throw new RestException('Could not upload file to the storage');
        }

        return $file->getId();
    }
}