<?php


namespace Likee\Sync;


class Handlers
{
    public static function deleteTask($iBoxTaskId)
    {
        $rsSyncTasks = \Likee\Sync\TaskTable::getList(
            array(
                'filter' => array('TASK_ID' => $iBoxTaskId)
            )
        );

        while ($rsTasks = $rsSyncTasks->fetch()) {
            $portal = \Likee\Sync\PortalManager::getById($rsTasks['PORTAL_ID']);
            $portal->deleteTask($rsTasks['PORTAL_TASK_ID']);
        }

        file_put_contents(
            $_SERVER['DOCUMENT_ROOT'] . '/synclogs/Handler.log',
            'START - ' . date('d.m.Y H:i:s') . ' deleted - ' . $iBoxTaskId . PHP_EOL,
            FILE_APPEND
        );
    }
}