<?php

namespace Likee\Sync;

use Bitrix\Main\Entity;

class FileTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'likee_sync_file';
    }

    public static function getFile()
    {
        return __FILE__;
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),

            new Entity\StringField('FILE_ID'),

            new Entity\StringField('PORTAL_ID'),

            new Entity\StringField('XML_ID')
        );
    }
}