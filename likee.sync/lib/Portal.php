<?php


namespace Likee\Sync;

use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\UserTable;
use Likee\Sync\Task\File;

Loader::includeModule('socialservices');
Loader::includeModule('tasks');
Loader::includeModule('forum');


class Portal
{
    private $isCloud = false;
    private $ID;
    private $fields;
    private $obTransport;

    const DATE_FORMAT = \DateTime::ATOM;
    const DATE_OFFSET = 3600 * 4;


    public function __construct($ID, $arFields = array())
    {
        $this->ID = $ID;

        if (empty($arFields))
            $arFields = PortalTable::getRowById($ID);

        $this->isCloud = $arFields['IS_CLOUD'] == 'Y';

        $this->fields = $arFields;

        if (!$this->isCloud)
            $this->fields['TOKEN'] = '';

        $this->obTransport = new \CBitrixPHPAppTransport($this->fields['TOKEN'], $this->fields['PORTAL']);
    }

    public function getTransport()
    {
        $this->refreshToken();
        return $this->obTransport;
    }

    public function isCloud()
    {
        return $this->isCloud;
    }

    /**
     * @param $iId
     * @return array|mixed
     */
    public function getUserById($iId)
    {
        if ($this->isCloud()) {
            $arResponse = $this->getTransport()->call('user.get', array(
                'FILTER' => array('ID' => $iId)
            ));

            if (!empty($arResponse['error']) || empty($arResponse['result']))
                return [];

            return reset($arResponse['result']);
        }

        $arUser = \CUser::GetByID($iId)->fetch();
        return $arUser;

    }

    public function getUserByEmail($sEmail)
    {
        if (empty($sEmail))
            return [];

        if ($this->isCloud()) {
            $arResponse = $this->getTransport()->call('user.get', array(
                'FILTER' => array('EMAIL' => $sEmail)
            ));

            if (!empty($arResponse['error']) || empty($arResponse['result']))
                return [];

            return reset($arResponse['result']);
        }

        $arUser = UserTable::getRow([
            'filter' => [
                'LOGIC' => 'OR',
                ['EMAIL' => $sEmail],
                ['LOGIN' => $sEmail]
            ]
        ]);

        return $arUser;
    }

    public function addTask(array $arTask, $iBoxTaskId = null)
    {
        if (empty($arTask['PORTAL_ID']))
            throw new \Exception('PORTAL_ID_NOT_FOUND');

        if ($arTask['PORTAL_ID'] == $this->getId())
            return $arTask['ID'];

        if (!$this->isCloud() && !empty($iBoxTaskId))
            return true;

        if ($this->isCloud() && empty($iBoxTaskId))
            throw new \Exception('BOX_TASK_ID_MISSING');

        if ($this->isCloud()) {
            $arFilterTask = array(
                'TASK_ID' => $iBoxTaskId,
                'PORTAL_ID' => $this->getId()
            );
        } else {
            $arFilterTask = array(
                'PORTAL_ID' => $arTask['PORTAL_ID'],
                'PORTAL_TASK_ID' => $arTask['ID']
            );
        }

        $arExistTask = TaskTable::getRow(array('filter' => $arFilterTask));

        if ($arExistTask)
            return $arExistTask['TASK_ID'];

        $arUser = $this->getUserByEmail($arTask['RESPONSIBLE_EMAIL']);

        if (empty($arUser))
            throw new \Exception('USER_NOT_FOUND');

        if (empty($arUser['ACTIVE'])) //если ACTIVE пуст значит пользователь уволен, для уволеного пользователя задача не создается
            throw new \Exception('USER_NOT_ACTIVE');


        $arCreatedByUser = $this->getUserByEmail($arTask['CREATED_BY_EMAIL']);

        if (empty($arCreatedByUser))
            throw new \Exception('CREATED_BY_USER_NOT_FOUND');

        $arPreparedFields = $this->prepareTaskFields($arTask);

        $arPreparedFields['RESPONSIBLE_ID'] = $arUser['ID'];
        $arPreparedFields['CREATED_BY'] = $arCreatedByUser['ID'];
        $arPreparedFields['STATUS'] = $arPreparedFields['REAL_STATUS'];
        unset($arPreparedFields['REAL_STATUS']);
        $arPreparedFields['DESCRIPTION'] = 'Задача с портала ' . $arTask['PORTAL_ADDRESS'] . PHP_EOL . $arTask['DESCRIPTION'];

        if ($this->isCloud()) {
            $arResponse = $this->getTransport()->call(
                'task.add',
                array('data' => $arPreparedFields)
            );
            $iTaskId = $arResponse['result']['ID'];

            if ($iTaskId <= 0)
                throw new \Exception('CLOUD_TASK_NOT_CREATE');

            $b = TaskTable::add(array(
                'TASK_ID' => $iBoxTaskId,
                'PORTAL_ID' => $this->getId(),
                'PORTAL_TASK_ID' => $iTaskId,
                'ORIGIN_PORTAL_ID' => $arTask['PORTAL_ID']
            ));

            if (!$b->isSuccess())
                throw new \Exception('CLOUD_TASK_NOT_CREATE_2');


        } else {
            $obTaskItem = \CTaskItem::add($arPreparedFields, 1);
            $iTaskId = intval($obTaskItem->getId());

            if ($iTaskId <= 0)
                throw new \Exception('BOX_TASK_NOT_CREATE');

            $b = TaskTable::add(array(
                'TASK_ID' => $obTaskItem->getId(),
                'PORTAL_ID' => $arTask['PORTAL_ID'],
                'PORTAL_TASK_ID' => $arTask['ID'],
                'ORIGIN_PORTAL_ID' => $arTask['PORTAL_ID']
            ));

            if (!$b->isSuccess())
                throw new \Exception('BOX_TASK_NOT_CREATE_2');
        }

        return $iTaskId;
    }

    public function updateTask(array $arTask, $iBoxTaskId)
    {
        if (empty($arTask['PORTAL_ID']))
            throw new \Exception('PORTAL_ID_NOT_FOUND');

        if ($arTask['PORTAL_ID'] == $this->getId())
            return true;

        if ($this->isCloud() && empty($iBoxTaskId))
            throw new \Exception('BOX_TASK_ID_MISSING');

        $arPreparedTaskFields = $this->prepareTaskFields($arTask);
        unset($arPreparedTaskFields['RESPONSIBLE_ID']);
        $arPreparedTaskFields['STATUS'] = $arPreparedTaskFields['REAL_STATUS'];
        unset($arPreparedTaskFields['REAL_STATUS']);
        unset($arPreparedTaskFields['CREATED_BY']);

        if (strpos($arPreparedTaskFields['DESCRIPTION'], 'Задача с портала') === false) {
            $iOriginPortalId = $this->getOriginPortalId($arTask);

            if ($iOriginPortalId != $this->getId()) {
                $obOriginPortal = PortalManager::getById($iOriginPortalId);

                if ($obOriginPortal && is_object($obOriginPortal))
                    $arPreparedTaskFields['DESCRIPTION'] = 'Задача с портала ' . $obOriginPortal->getField('PORTAL') . PHP_EOL . $arPreparedTaskFields['DESCRIPTION'];
            }
        }

        if ($this->isCloud()) {
            $arFilterTask = array(
                'TASK_ID' => $iBoxTaskId,
                'PORTAL_ID' => $this->getId()
            );

            $arSync = TaskTable::getRow(array('filter' => $arFilterTask));

            if (!$arSync)
                return false;

            $arResponse = $this->getTransport()->call(
                'task.item.update',
                array(
                    $arSync['PORTAL_TASK_ID'],
                    'data' => $arPreparedTaskFields
                )
            );

            if (!empty($arResponse['error'])) {
                echo print_r($arResponse['error'], true) . PHP_EOL;
            }

            return empty($arResponse['error']) ? $arSync['PORTAL_TASK_ID'] : 0;
        } else {
            $arPreparedTaskFields['ID'] = $iBoxTaskId;

            $obTaskItem = new \CTaskItem($iBoxTaskId, 1);
            $obTaskItem->update($arPreparedTaskFields);

            return $iBoxTaskId;
        }
    }

    /**
     * @param $arTask
     * @return int
     */
    public function getBoxTaskId($arTask)
    {
        if ($arTask['PORTAL_ID'] == PortalManager::getBox()->getId())
            return intval($arTask['ID']);

        $arTaskSync = TaskTable::getRow(array(
            'filter' => array(
                'PORTAL_ID' => $arTask['PORTAL_ID'],
                'PORTAL_TASK_ID' => $arTask['ID']
            )
        ));

        return intval($arTaskSync['TASK_ID']);
    }

    public function getOriginPortalId($arTask)
    {
        if ($this->isCloud()) {
            if ($arTask['PORTAL_ID'] == PortalManager::getBox()->getId()) {
                $arFilterTask = array(
                    'TASK_ID' => $arTask['ID'],
                    'PORTAL_ID' => $this->getId()
                );
            } else {
                if (empty($iBoxTaskId))
                    return false;

                $arFilterTask = array(
                    'TASK_ID' => $iBoxTaskId,
                    'PORTAL_ID' => $this->getId()
                );
            }
        } else {
            $arFilterTask = array(
                'PORTAL_TASK_ID' => $arTask['ID'],
                'PORTAL_ID' => $arTask['PORTAL_ID']
            );
        }

        $arTaskSync = TaskTable::getRow(['filter' => $arFilterTask]);

        return intval($arTaskSync['ORIGIN_PORTAL_ID']);
    }

    public function getBoxFileId($arFile)
    {
        if ($arFile['PORTAL_ID'] == PortalManager::getBox()->getId())
            return intval($arFile['ID']);

        $arFileSync = FileTable::getRow(array(
            'filter' => array(
                'PORTAL_ID' => $arFile['PORTAL_ID'],
                'XML_ID' => $arFile['ID']
            )
        ));

        return intval($arFileSync['FILE_ID']);
    }

    public function taskExist($arTask, $iBoxTaskId = null)
    {
        if (empty($arTask['PORTAL_ID']))
            throw new \Exception('PORTAL_ID_NOT_FOUND');

        if ($arTask['PORTAL_ID'] == $this->getId())
            return true;


        if ($this->isCloud()) {
            if ($arTask['PORTAL_ID'] == PortalManager::getBox()->getId()) {
                $arFilterTask = array(
                    'TASK_ID' => $arTask['ID'],
                    'PORTAL_ID' => $this->getId()
                );
            } else {
                if (empty($iBoxTaskId))
                    return false;

                $arFilterTask = array(
                    'TASK_ID' => $iBoxTaskId,
                    'PORTAL_ID' => $this->getId()
                );
            }
        } else {
            $arFilterTask = array(
                'PORTAL_TASK_ID' => $arTask['ID'],
                'PORTAL_ID' => $arTask['PORTAL_ID']
            );
        }

        return TaskTable::getCount($arFilterTask) > 0;
    }

    public function fileExist($arFile, $iBoxFileId = null)
    {
        if (empty($arFile['PORTAL_ID']))
            throw new \Exception('PORTAL_ID_NOT_FOUND');

        if ($arFile['PORTAL_ID'] == $this->getId())
            return true;


        if ($this->isCloud()) {
            if (PortalManager::getById($arFile['PORTAL_ID'])->isCloud()) {
                if (empty($iBoxFileId))
                    return false;

                $arFileTask = array(
                    'FILE_ID' => $iBoxFileId,
                    'PORTAL_ID' => $this->getId()
                );
            } else {
                $arFileTask = array(
                    'FILE_ID' => $arFile['ID'],
                    'PORTAL_ID' => $this->getId()
                );
            }
        } else {
            $arFileTask = array(
                'XML_ID' => $arFile['ID'],
                'PORTAL_ID' => $arFile['PORTAL_ID']
            );
        }

        return FileTable::getCount($arFileTask) > 0;
    }

    public function deleteTask($iTaskId)
    {
        if ($this->isCloud()) {
            $this->getTransport()->call(
                'task.item.delete',
                array($iTaskId)
            );
        } else {
            $obTask = \CTaskItem::getInstance($iTaskId, 1);
            if ($obTask && is_object($obTask))
                $obTask->delete();
        }
    }

    public function getTasks($iTime)
    {
        global $DB;

        $arTasks = [];

        if ($this->isCloud()) {
            $obDate = DateTime::createFromTimestamp($iTime); // + self::DATE_OFFSET

            $arRequest = $this->getTransport()->call(
                'task.item.list',
                array(
                    'ORDER' => array('CHANGED_DATE' => 'ASC'),
                    'FILTER' => array('>CHANGED_DATE' => $obDate->format(self::DATE_FORMAT))
                )
            );

            $arTasks = $arRequest['result'];
        } else {
            $rsLocalTasks = \CTasks::GetList(
                array('CHANGED_DATE' => 'ASC'),
                array(
                    '>CHANGED_DATE' => date($DB->DateFormatToPHP(\CSite::GetDateFormat('FULL')), $iTime),
                    'CHECK_PERMISSIONS' => 'N'
                )
            );

            while ($arTask = $rsLocalTasks->Fetch()) {
                //спизженно из \CTasks::OnSearchReindex();
                $rsMembers = \CTaskMembers::GetList(array(), array('TASK_ID' => $arTask['ID']));
                while ($arMember = $rsMembers->Fetch()) {
                    if ($arMember['TYPE'] == 'A') {
                        $arTask['ACCOMPLICES'][] = $arMember['USER_ID'];
                    } elseif ($arMember['TYPE'] == 'U') {
                        $arTask['AUDITORS'][] = $arMember['USER_ID'];
                    }
                }

                $arTasks[] = $arTask;
            }
        }

        foreach ($arTasks as &$arTask) {
            $arTask['PORTAL_ID'] = $this->getId();
            $arTask['PORTAL_ADDRESS'] = $this->getField('PORTAL');

            $arUser = $this->getUserById($arTask['RESPONSIBLE_ID']);
            $arTask['RESPONSIBLE_EMAIL'] = $arUser['EMAIL'];

            $arCreatedByUser = $this->getUserById($arTask['CREATED_BY']);
            $arTask['CREATED_BY_EMAIL'] = $arCreatedByUser['EMAIL'];

            /**
             * ACCOMPLICES AND AUDITORS
             * Соисполнители и Наблюдатели
             */

            $arTask['ACCOMPLICES_EMAILS'] = $arTask['AUDITORS_EMAILS'] = array();

            foreach ($arTask['AUDITORS'] as $iAuditorId) {
                if ($arUser = $this->getUserById($iAuditorId))
                    $arTask['AUDITORS_EMAILS'][] = $arUser['EMAIL'];
            }

            foreach ($arTask['ACCOMPLICES'] as $iAuditorId) {
                if ($arUser = $this->getUserById($iAuditorId))
                    $arTask['ACCOMPLICES_EMAILS'][] = $arUser['EMAIL'];
            }

            $arTask['AUDITORS'] = $arTask['ACCOMPLICES'] = array();

            /** FILES */
            $arTask['FILES'] = File::getTaskFiles($this, $arTask['ID']);
        }
        unset($arTask);

        return $arTasks;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getField($sField)
    {
        return $this->fields[$sField];
    }

    public function prepareTaskFields($arTask)
    {
        $arFields = array(
            'TITLE',                //Название задачи
            'DESCRIPTION',          //Описание задачи
            'DEADLINE',             //Крайний срок
            'REAL_STATUS',          //Статус
            'PRIORITY',             //Приоритет
            'TAGS',                 //Теги
            'ALLOW_CHANGE_DEADLINE',//Разрешить ответственному менять крайний срок
            'TASK_CONTROL',         //Принять работу после завершения задачи
            'RESPONSIBLE_ID',       //Идентификатор ответственного
            'TIME_ESTIMATE',        //Плановые трудозатраты
            'DECLINE_REASON',       //Причина отклонения задачи
            'DURATION_FACT',        //Затраченное время
            'DURATION_PLAN',        //Оценка
            'DURATION_TYPE',        //Тип единицы измерения в планируемой длительности: days или hours
            //'CLOSED_DATE',          //Дата завершения
            'TIME_SPENT_IN_LOGS',   //Затраченное время на задачу
            'ALLOW_TIME_TRACKING',  //Флаг включения учета затраченного времени по задаче
            'MATCH_WORK_TIME'       //Флаг, который показывает, что даты исполнения и крайний срок должны всегда устанавливаться в рабочее время
        );

        $arResult = array_intersect_key($arTask, array_flip($arFields));

        $arResult = array_filter($arResult, function ($sValue) {
            return strlen($sValue) > 0;
        });

        $arResult['DEADLINE'] = $this->convertDate($arTask, 'DEADLINE');

        /**
         * ACCOMPLICES AND AUDITORS
         * Соисполнители и Наблюдатели
         */
        foreach ($arTask['AUDITORS_EMAILS'] as $sUserEmail) {
            if ($arUser = $this->getUserByEmail($sUserEmail))
                $arResult['AUDITORS'][] = $arUser['ID'];
        }

        foreach ($arTask['ACCOMPLICES_EMAILS'] as $sUserEmail) {
            if ($arUser = $this->getUserByEmail($sUserEmail))
                $arResult['ACCOMPLICES'][] = $arUser['ID'];
        }

        return $arResult;
    }

    /**
     * @param $arTask
     * @param $sField
     * @return false|string
     */
    private function convertDate($arTask, $sField)
    {
        global $DB;

        if (empty($arTask[$sField]))
            return '';

        $iTimeStamp = 0;

        $obPortalFrom = PortalManager::getById($arTask['PORTAL_ID']);

        if ($obPortalFrom->isCloud()) {
            $obDate = \DateTime::createFromFormat(\DateTime::ATOM, $arTask[$sField]);
            if ($obDate && is_object($obDate))
                $iTimeStamp = $obDate->getTimestamp();
        } else {
            $iTimeStamp = MakeTimeStamp($arTask[$sField]);
        }

        if (!$iTimeStamp || $iTimeStamp <= 0)
            return '';

        //@todo вынести DATE_OFFSET в параметры портала

        //из коробки в levita.bitrix24
        if (!$obPortalFrom->isCloud() && $this->getId() == 3)
            $iTimeStamp -= self::DATE_OFFSET;

        //из fakel в коробку
        if ($obPortalFrom->getId() == 2 && !$this->isCloud())
            $iTimeStamp += self::DATE_OFFSET;

        //из levita
        if ($obPortalFrom->getId() == 3)
            $iTimeStamp += self::DATE_OFFSET;

        if ($this->isCloud()) {
            return DateTime::createFromTimestamp($iTimeStamp)->format(self::DATE_FORMAT);
        } else {
            return date($DB->DateFormatToPHP(\CSite::GetDateFormat('FULL')), $iTimeStamp);
        }
    }

    public function getDefaultUserEmail()
    {
        return $this->fields['DEFAULT_EMAIL'];
    }

    public function getToken($code)
    {
        $arGetParams = array(
            'grant_type' => 'authorization_code',
            'client_id' => $this->fields['CLIENT_ID'],
            'client_secret' => $this->fields['CLIENT_SECRET'],
            'code' => $code
        );

        $sTokenRequestUrl = 'https://oauth.bitrix.info/oauth/token/?' . http_build_query($arGetParams);

        return OAuth::sendRequest($sTokenRequestUrl);
    }

    public function refreshToken()
    {
        if (!$this->isCloud())
            return true;

        /** @var DateTime $obExpDate */
        $obExpDate = $this->fields['TOKEN_EXPIRE_DATE'];

        if ($obExpDate > new DateTime())
            return true;

        $arGetParams = array(
            'grant_type' => 'refresh_token',
            'client_id' => $this->fields['CLIENT_ID'],
            'client_secret' => $this->fields['CLIENT_SECRET'],
            'refresh_token' => $this->fields['REFRESH_TOKEN']
        );

        $sTokenRequestUrl = 'https://oauth.bitrix.info/oauth/token/?' . http_build_query($arGetParams);

        $arTokenData = OAuth::sendRequest($sTokenRequestUrl);

        if (!$arTokenData)
            return false;

        $this->fields['TOKEN'] = $arTokenData['access_token'];
        $this->fields['REFRESH_TOKEN'] = $arTokenData['refresh_token'];
        $this->fields['TOKEN_EXPIRE_DATE'] = (new DateTime())->add(intval($arTokenData['expires_in']) . ' seconds');

        $b = $this->save()->isSuccess();

        file_put_contents(
            $_SERVER['DOCUMENT_ROOT'] . '/refreshToken.log',
            date('d.m.Y H:i:s') . ' - portal {$this->getId()} status - ' . ($b ? 'Y' : 'N') . PHP_EOL,
            FILE_APPEND
        );

        return $b;
    }

    private function save()
    {
        return $this->update($this->fields);
    }

    public function update(array $arFields)
    {
        return PortalTable::update($this->ID, $arFields);
    }

    public function getId()
    {
        return $this->ID;
    }


    public function getComments($iTaskId, $iTime)
    {
        $iTaskId = intval($iTaskId);

        $arComments = array();

        if ($iTaskId <= 0)
            return $arComments;

        if ($this->isCloud()) {
            $obDate = DateTime::createFromTimestamp($iTime + self::DATE_OFFSET);


            $arRequest = $this->getTransport()->call(
                'task.commentitem.getlist',
                array(
                    'TASKID' => $iTaskId,
                    'ORDER' => array('POST_DATE' => 'DESC'),
                    'FILTER' => array('>POST_DATE' => $obDate->format(self::DATE_FORMAT))
                )
            );

            $arComments = $arRequest['result'];
        } else {
            global $DB;

            $arTask = \CTaskItem::getInstance($iTaskId, 1)->getData();

            $iForumId = intval($arTask['FORUM_ID']);
            $iForumTopicId = intval($arTask['FORUM_TOPIC_ID']);

            if ($iForumId <= 0 || $iForumTopicId <= 0)
                return $arComments;

            $rsComments = \CForumMessage::GetList(
                array('POST_DATE' => 'ASC'),
                array(
                    'FORUM_ID' => $iForumId,
                    'TOPIC_ID' => $iForumTopicId,
                    'NEW_TOPIC' => 'N',
                    '>POST_DATE' => date($DB->DateFormatToPHP(\CSite::GetDateFormat('FULL')), $iTime)
                )
            );


            while ($arComment = $rsComments->Fetch()) {
                $arComments[] = $arComment;
            }
        }

        foreach ($arComments as &$arComment) {
            $arUser = $this->getUserById($arComment['AUTHOR_ID']);
            $arComment['USER_EMAIL'] = $arUser['EMAIL'];
            $arComment['PORTAL_ID'] = $this->getId();
            $arComment['PORTAL_TASK_ID'] = $iTaskId;
        }
        unset($arComment);

        return $arComments;
    }

    public function addComment($arComment)
    {
        if ($arComment['PORTAL_ID'] == $this->getId())
            return true;

        if (empty($arComment['POST_MESSAGE']))
            return false;

        // при создании топика форума создается комментарий с названием и описанием топика, его надо пропускать
        $obTaskPortal = PortalManager::getById($arComment['PORTAL_ID']);
        if ($obTaskPortal->isCloud()) { //в коробке мы такие комментарии фильтруем при выборке, сюда они не попадут
            $arTask = $obTaskPortal->getTransport()->call('task.item.getdata', ['TASKID' => $arComment['PORTAL_TASK_ID']]);

            if (!$arTask)
                return false;

            if (strpos($arComment['POST_MESSAGE'], $arTask['result']['TITLE']) !== false)
                return true;
        }

        if (
            !empty($arComment['AUTHOR_NAME'])
            && strpos($arComment['POST_MESSAGE'], $arComment['AUTHOR_NAME']) === false
            && strpos($arComment['POST_MESSAGE'], $arComment['USER_EMAIL']) === false
        ) {
            $arComment['POST_MESSAGE'] = '(От: ' . $arComment['AUTHOR_NAME'] . ') ' . PHP_EOL . $arComment['POST_MESSAGE'];
        } else if (strpos($arComment['POST_MESSAGE'], $arComment['USER_EMAIL']) === false) {
            $arComment['POST_MESSAGE'] = '(От: ' . $arComment['USER_EMAIL'] . ') ' . PHP_EOL . $arComment['POST_MESSAGE'];
        }

        if ($this->isCloud()) {
            if ($arComment['PORTAL_ID'] == PortalManager::getBox()->getId()) {
                $arFilterSync = array(
                    'TASK_ID' => $arComment['PORTAL_TASK_ID'],
                    'PORTAL_ID' => $this->getId()
                );
            } else {
                $arFilterSync = array(
                    'PORTAL_ID' => $arComment['PORTAL_ID'],
                    'PORTAL_TASK_ID' => $arComment['PORTAL_TASK_ID']
                );
            }

            $arSync = TaskTable::getRow(array('filter' => $arFilterSync));

            if (!$arSync)
                return false;

            $arFilterSync = array(
                'TASK_ID' => $arSync['TASK_ID'],
                'PORTAL_ID' => $this->getId()
            );

            $arSync = TaskTable::getRow(array('filter' => $arFilterSync));

            if (!$arSync)
                return false;

            $arResponse = $this->getTransport()->call(
                'task.commentitem.add',
                array(
                    'TASKID' => $arSync['PORTAL_TASK_ID'],
                    'FIELDS' => array(
                        'POST_MESSAGE' => $arComment['POST_MESSAGE']
                    )
                )
            );

            return intval($arResponse['result']);
        } else {
            if ($arComment['PORTAL_ID'] == $this->getId())
                $iTaskId = $arComment['PORTAL_TASK_ID'];
            else {
                $arFilterSync = array(
                    'PORTAL_ID' => $arComment['PORTAL_ID'],
                    'PORTAL_TASK_ID' => $arComment['PORTAL_TASK_ID']
                );
                $arSync = TaskTable::getRow(array('filter' => $arFilterSync));
                $iTaskId = $arSync['TASK_ID'];
            }

            $obTask = \CTaskItem::getInstance($iTaskId, 1);
            $arTask = $obTask->getData();

            $iForumId = intval($arTask['FORUM_ID']) > 0 ? intval($arTask['FORUM_ID']) : $this->getForumIdForBox();
            $iForumTopicId = intval($arTask['FORUM_TOPIC_ID']);

            if ($iForumId <= 0)
                return false;

            if ($iForumTopicId <= 0) {
                $arUserAdmin = \CUser::GetByID(1)->Fetch();

                $arTopicFields = array(
                    'TITLE' => $arTask['~TITLE'],
                    'FORUM_ID' => $iForumId,
                    'USER_START_ID' => $arUserAdmin['ID'],
                    'USER_START_NAME' => $arUserAdmin['EMAIL'],
                    'LAST_POSTER_NAME' => $arUserAdmin['EMAIL'],
                    'APPROVED' => 'Y',
                    'PERMISSION_EXTERNAL' => 'Y',
                    'PERMISSION' => 'Y',
                    'NAME_TEMPLATE' => \CSite::GetNameFormat(false),
                    'XML_ID' => 'TASK_' . $iTaskId
                );

                $iForumTopicId = \CForumTopic::Add($arTopicFields);

                if ($iForumTopicId > 0)
                    $obTask->update(array('FORUM_TOPIC_ID' => $iForumTopicId));
            }

            if ($iForumTopicId <= 0)
                return false;

            $arUser = $this->getUserByEmail($arComment['USER_EMAIL']);

            if (!$arUser)
                $arUser = $this->getUserByEmail($this->getDefaultUserEmail());

            $arNewComment = array(
                'POST_MESSAGE' => $arComment['POST_MESSAGE'],
                'AUTHOR_NAME' => $arComment['AUTHOR_NAME'],
                'TOPIC_ID' => $iForumTopicId,
                'FORUM_ID' => $iForumId
            );

            if ($arUser)
                $arNewComment['AUTHOR_ID'] = $arUser['ID'];

            $bResult = \CForumMessage::Add($arNewComment);

            return $bResult !== false;
        }
    }

    public function getBoxCheckListId($arCheckList)
    {
        if ($arCheckList['PORTAL_ID'] == PortalManager::getBox()->getId())
            return intval($arCheckList['ID']);

        $arTaskSync = CheckListTable::getRow(array(
            'filter' => array(
                'PORTAL_ID' => $arCheckList['PORTAL_ID'],
                'PORTAL_CHECKLIST_ID' => $arCheckList['ID']
            )
        ));

        return intval($arTaskSync['CHECKLIST_ID']);
    }

    public function CheckListExist($arCheckList, $iBoxTaskId = null)
    {
        if (empty($arCheckList['PORTAL_ID']))
            throw new \Exception('PORTAL_ID_NOT_FOUND');

        if ($arCheckList['PORTAL_ID'] == $this->getId())
            return true;


        if ($this->isCloud()) {
            if ($arCheckList['PORTAL_ID'] == PortalManager::getBox()->getId()) {
                $arFilterTask = array(
                    'CHECKLIST_ID' => $arCheckList['ID'],
                    'PORTAL_ID' => $this->getId()
                );
            } else {
                if (empty($iBoxTaskId))
                    return false;

                $arFilterTask = array(
                    'CHECKLIST_ID' => $iBoxTaskId,
                    'PORTAL_ID' => $this->getId()
                );
            }
        } else {
            $arFilterTask = array(
                'PORTAL_CHECKLIST_ID' => $arCheckList['ID'],
                'PORTAL_ID' => $arCheckList['PORTAL_ID']
            );
        }

        return CheckListTable::getCount($arFilterTask) > 0;
    }


    public function getCheckLists($iTaskId)
    {
        $iTaskId = intval($iTaskId);

        $arCheckLists = array();

        if ($iTaskId <= 0)
            return $arCheckLists;

        if ($this->isCloud()) {

            $arRequest = $this->getTransport()->call(
                'task.checklistitem.getlist',
                array($iTaskId, array('TOGGLED_DATE' => 'desc'))
            );

            $arCheckLists = $arRequest['result'];

        } else {

            $rsCheckLists = \CTaskCheckListItem::getByTaskId($iTaskId);

            while ($arCheckList = $rsCheckLists->Fetch()) {
                $arCheckLists[] = $arCheckList;
            }
        }

        foreach ($arCheckLists as &$arCheckList) {
            $arCheckList['PORTAL_ID'] = $this->getId();
            $arCheckList['PORTAL_TASK_ID'] = $iTaskId;
        }
        unset($arCheckList);

        return $arCheckLists;
    }

    public function addCheckList($arCheckList, $iBoxCheckListId = null)
    {
        if (empty($arCheckList['PORTAL_ID']))
            throw new \Exception('PORTAL_ID_NOT_FOUND');

        if ($arCheckList['PORTAL_ID'] == $this->getId())
            return $arCheckList['ID'];

        if (!$this->isCloud() && !empty($iBoxTaskId))
            return true;

        if ($this->isCloud()) {
            $arFilterTask = array(
                'PORTAL_CHECKLIST_ID' => $arCheckList['ID'],
                'PORTAL_ID' => $this->getId()
            );
        } else {
            $arFilterTask = array(
                'CHECKLIST_ID' => $arCheckList['ID'],
                'PORTAL_CHECKLIST_ID' => $arCheckList['ID']
            );
        }

        $arExistCheckList = CheckListTable::getRow(array('filter' => $arFilterTask));

        if ($arExistCheckList)
            return $arExistCheckList['CHECKLIST_ID'];

        $arUser = $this->getUserByEmail($arCheckList['CREATED_BY']);

        if (empty($arUser))
            $arUser = $this->getUserByEmail($this->getDefaultUserEmail());

        if (empty($arUser))
            throw new \Exception('USER_NOT_FOUND');

        if ($arCheckList['PORTAL_ID'] != PortalManager::getBox()->getId()) {
            $arFilterSync = array(
                'PORTAL_ID' => $arCheckList['PORTAL_ID'],
                'PORTAL_TASK_ID' => $arCheckList['PORTAL_TASK_ID']
            );
        } else {
            $arFilterSync = array(
                'TASK_ID' => $arCheckList['PORTAL_TASK_ID'],
                'PORTAL_ID' => $this->getId()
            );
        }

        $arSync = TaskTable::getRow(array('filter' => $arFilterSync));

        if (!$arSync)
            return false;

        if ($this->isCloud()) {
            $arResponse = $this->getTransport()->call(
                'task.checklistitem.add',
                array(
                    'TASKID' => $arSync['PORTAL_TASK_ID'],
                    'FIELDS' => array(
                        'TITLE' => $arCheckList['TITLE'],
                        'IS_COMPLETE' => $arCheckList['IS_COMPLETE']
                    )
                )
            );

            $iCheckListId = $arResponse['result']['ID'];

            if ($iCheckListId <= 0)
                throw new \Exception('CLOUD_TASK_NOT_CREATE');

            $b = CheckListTable::add(array(
                'CHECKLIST_ID' => $iBoxCheckListId,
                'PORTAL_ID' => $this->getId(),
                'PORTAL_CHECKLIST_ID' => $iCheckListId
            ));

            if (!$b->isSuccess())
                throw new \Exception('CLOUD_TASK_NOT_CREATE_2');

            return $iCheckListId;
        } else {
            $obCheckListItem = \CTaskCheckListItem::add(
                \CTaskItem::getInstance($arSync['TASK_ID'], 1),
                array(
                    'TITLE' => $arCheckList['TITLE'],
                    'IS_COMPLETE' => $arCheckList['IS_COMPLETE']
                )
            );

            if ($obCheckListItem->getId() <= 0)
                throw new \Exception('BOX_TASK_NOT_CREATE');

            $b = CheckListTable::add(array(
                'CHECKLIST_ID' => $obCheckListItem->getId(),
                'PORTAL_ID' => $arCheckList['PORTAL_ID'],
                'PORTAL_CHECKLIST_ID' => $arCheckList['ID']
            ));

            if (!$b->isSuccess())
                throw new \Exception('BOX_TASK_NOT_CREATE_2');

            return $obCheckListItem->getId();
        }
    }

    public function updateCheckList(array $arCheckList, $iBoxCheckListId)
    {
        if (empty($arCheckList['PORTAL_ID']))
            throw new \Exception('PORTAL_ID_NOT_FOUND');

        if ($arCheckList['PORTAL_ID'] == $this->getId())
            return true;

        if ($this->isCloud() && empty($iBoxCheckListId))
            throw new \Exception('BOX_TASK_ID_MISSING');

        if ($arCheckList['PORTAL_ID'] != PortalManager::getBox()->getId()) {
            $arFilterSync = array(
                'PORTAL_ID' => $arCheckList['PORTAL_ID'],
                'PORTAL_TASK_ID' => $arCheckList['PORTAL_TASK_ID']
            );
        } else {
            $arFilterSync = array(
                'TASK_ID' => $arCheckList['PORTAL_TASK_ID'],
                'PORTAL_ID' => $this->getId()
            );
        }

        $arSyncTask = TaskTable::getRow(array('filter' => $arFilterSync));

        if (!$arSyncTask)
            return false;


        if ($this->isCloud()) {
            $arFilterTask = array(
                'CHECKLIST_ID' => $iBoxCheckListId,
                'PORTAL_ID' => $this->getId()
            );

            $arSync = CheckListTable::getRow(array('filter' => $arFilterTask));

            if (!$arSync)
                return false;

            $arResponse = $this->getTransport()->call(
                'task.checklistitem.update',
                array(
                    $arSyncTask['PORTAL_TASK_ID'],
                    $arSync['PORTAL_CHECKLIST_ID'],
                    array('IS_COMPLETE' => $arCheckList['IS_COMPLETE'])
                )
            );


            return empty($arResponse['error']);
        } else {

            $obTaskItem = new \CTaskCheckListItem(\CTaskItem::getInstance($arSyncTask['TASK_ID'], 1), $iBoxCheckListId);
            $obTaskItem->update(array('IS_COMPLETE' => $arCheckList['IS_COMPLETE']));

            return true;
        }
    }

    public static function getForumIdForBox()
    {
        try {
            $iForumId = \CTasksTools::getForumIdForIntranet();
        } catch (\Exception $e) {
            $iForumId = 0;
        }

        return $iForumId;
    }
}