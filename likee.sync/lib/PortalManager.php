<?php


namespace Likee\Sync;


class PortalManager
{
    private static $iBoxPortalId = 1;
    private static $portals = null;
    private static $filter_active = true;

    /**
     * @return Portal[]
     */
    public static function getAll()
    {
        if (is_null(self::$portals)) {
            $arFilter = array();

            if (self::$filter_active)
                $arFilter['ACTIVE'] = 'Y';

            $rsPortals = PortalTable::getList(array(
                'filter' => $arFilter
            ));

            while ($arRow = $rsPortals->fetch()) {
                self::$portals[$arRow['ID']] = new Portal($arRow['ID'], $arRow);
            }
        }

        return self::$portals;
    }

    public static function getBox()
    {
        return self::getById(self::$iBoxPortalId);
    }

    /**
     * @param $id
     * @return Portal
     */
    public static function getById($id)
    {
        $id = intval($id);

        self::getAll();

        if (!array_key_exists($id, self::$portals))
            return null;

        return self::$portals[$id];
    }

    public static function getByDomain($sDomain)
    {
        $arPortal = PortalTable::getRow(array(
            'filter' => array('%PORTAL' => $sDomain)
        ));

        return $arPortal ? self::getById($arPortal['ID']) : null;
    }

    public static function setFilterActive($bActive)
    {
        self::$filter_active = boolval($bActive);
    }
}