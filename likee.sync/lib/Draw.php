<?php

namespace Likee\Sync;


class Draw
{
    public static function adminLabel($sTitle = '', $bRequired = false, $bTopAlign = false)
    {
        $sLabel = '<td width="40%;" class="adm-detail-content-cell-l';
        if ($bTopAlign)
            $sLabel .= ' adm-detail-valign-top';
        $sLabel .= '">';

        if ($bRequired)
            $sLabel .= '<span class="adm-required-field">';
        $sLabel .= htmlspecialcharsbx($sTitle) . ':';
        if ($bRequired)
            $sLabel .= '</span>';
        $sLabel .= '</td>';

        return $sLabel;
    }

    private static function adminParams($arParams = [])
    {
        $sParams = '';

        if (!$arParams)
            return $sParams;

        foreach ($arParams as $sKey => $sVal) {
            if (is_numeric($sKey)) {
                $sParams .= ' ' . $sVal;
            } else {
                $sParams .= ' ' . $sKey . '="' . $sVal . '"';
            }
        }

        return $sParams;
    }

    public static function adminHeading($sHeading = '')
    {
        ob_start();
        ?>
        <tr class="heading">
            <td colspan="2"><b><?= $sHeading; ?></b></td>
        </tr>
        <?
        return ob_get_clean();
    }

    public static function adminTextField($sName = '', $sTitle = '', $bRequired = false, $sValue = '', $arParams = array())
    {
        ob_start();
        ?>
        <tr>
            <?= self::adminLabel($sTitle, $bRequired); ?>
            <td class="adm-detail-content-cell-r">
                <input type="text" name="<?= htmlspecialcharsbx($sName); ?>" value="<?= htmlspecialcharsbx($sValue); ?>" title="<?= htmlspecialcharsbx($sTitle); ?>"<?= self::adminParams($arParams); ?>>
            </td>
        </tr>
        <?
        return ob_get_clean();
    }

    public static function adminTextAreaField($sName = '', $sTitle = '', $bRequired = false, $sValue = '', $arParams = array())
    {
        if (empty($arParams['cols'])) {
            $arParams['cols'] = 52;
        }
        if (empty($arParams['rows'])) {
            $arParams['rows'] = 10;
        }
        ob_start();
        ?>
        <tr>
            <?= self::adminLabel($sTitle, $bRequired, true); ?>
            <td class="adm-detail-content-cell-r">
                <textarea name="<?= htmlspecialcharsbx($sName); ?>"<?= self::adminParams($arParams); ?>><?= htmlspecialcharsbx($sValue); ?></textarea>
            </td>
        </tr>
        <?
        return ob_get_clean();
    }

    public static function adminCheckboxField($sName = '', $sTitle = '', $bRequired = false, $sValue = 'Y', $bChecked = false, $arParams = array(), $sEmptyValue = null)
    {
        if ($bChecked && !in_array('checked', $arParams))
            $arParams[] = 'checked';

        ob_start();
        ?>
        <tr>
            <?= self::adminLabel($sTitle, $bRequired); ?>
            <td class="adm-detail-content-cell-r">
                <? if (!is_null($sEmptyValue)): ?>
                    <input type="hidden" name="<?= $sName; ?>" value="<?= $sEmptyValue; ?>">
                <? endif; ?>
                <input type="checkbox" name="<?= $sName; ?>" value="<?= $sValue; ?>" title="<?= $sTitle; ?>"<?= self::adminParams($arParams); ?>>
            </td>
        </tr>
        <?
        return ob_get_clean();
    }

    public static function adminViewField($sName = '', $sTitle = '', $sValue = '')
    {
        ob_start();
        ?>
        <tr>
            <?= self::adminLabel($sTitle); ?>
            <td class="adm-detail-content-cell-r"><?= htmlspecialcharsbx($sValue); ?></td>
        </tr>
        <?
        return ob_get_clean();
    }

    public static function adminSelectFiled($sName = '', $sTitle = '', $bRequired = false, $arValues = array(), $sSelected = '', $arParams = array())
    {
        ob_start();
        ?>
        <tr>
            <?= self::adminLabel($sTitle, $bRequired); ?>
            <td class="adm-detail-content-cell-r">
                <select name="<?= $sName; ?>" title="<?= $sTitle; ?>"<?= self::adminParams($arParams); ?>>
                    <? foreach ($arValues as $sValue => $sValueTitle): ?>
                        <option value="<?= $sValue; ?>"<?= $sValue == $sSelected ? ' selected' : ''; ?>><?= $sValueTitle; ?></option>
                    <? endforeach; ?>
                </select>
            </td>
        </tr>
        <?
        return ob_get_clean();
    }
}