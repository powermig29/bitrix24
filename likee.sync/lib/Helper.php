<?php
/**
 * User: Azovcev Artem
 * Date: 14.09.17
 * Time: 9:58
 */

namespace Likee\Sync;


use Bitrix\Main\Config\Option;

class Helper
{
    public static function deleteData()
    {
        $sUseWebHook = Option::get('likee.synk', 'use_webhook', 'Y');

        if ($sUseWebHook === 'Y')
            Option::set('likee.synk', 'use_webhook', 'N');

        $arPortals = PortalManager::getAll();

        $iTime = MakeTimeStamp('01.09.2017 00:00:00');

        foreach ($arPortals as $obPortal) {
            $arTasks = $obPortal->getTasks($iTime);
            foreach ($arTasks as $arTask)
                $obPortal->deleteTask($arTask['ID']);
        }

        $rsRows = TaskTable::getList();
        while ($arRow = $rsRows->fetch())
            TaskTable::delete($arRow['ID']);

        $rsRows = FileTable::getList();
        while ($arRow = $rsRows->fetch())
            FileTable::delete($arRow['ID']);

        if ($sUseWebHook === 'Y')
            Option::set('likee.synk', 'use_webhook', 'Y');
    }
}