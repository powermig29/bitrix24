<?php

namespace Likee\Sync;

use Bitrix\Main\Entity;

class TaskTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'likee_sync_task';
    }

    public static function getFile()
    {
        return __FILE__;
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),

            new Entity\StringField('TASK_ID'),

            new Entity\StringField('PORTAL_ID'),

            new Entity\StringField('PORTAL_TASK_ID'),

            new Entity\StringField('ORIGIN_PORTAL_ID')
        );
    }
}