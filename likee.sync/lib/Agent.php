<?php


namespace Likee\Sync;

use Likee\Sync\Task\File;

class Agent
{
    public static function syncTasks($iTime = 0)
    {
        $iTime = intval($iTime);

        if ($iTime <= 0)
            $iTime = time() - 60;

        $arPortals = PortalManager::getAll();

        if (!$arPortals)
            return __METHOD__ . '(' . time() . ');';

        $arTasks = array();

        $obBoxPortal = PortalManager::getBox();

        foreach ($arPortals as $obPortal) {
            $arTasks = array_merge($arTasks, $obPortal->getTasks($iTime));
        }

        file_put_contents(
            $_SERVER['DOCUMENT_ROOT'] . '/synclogs/newTasks.log',
            date('d.m.Y H:i:s') . "new task " . print_r($arTasks, true) . PHP_EOL . '=====================' . PHP_EOL,
            FILE_APPEND
        );

        file_put_contents(
            $_SERVER['DOCUMENT_ROOT'] . '/synclogs/sync.log',
            str_repeat('=', 25) . 'AGENT START ' . date('d.m.Y H:i:s') . str_repeat('=', 25) . PHP_EOL .
            'COUNT TASKS - ' . count($arTasks) . PHP_EOL .
            'SELECT TIME FROM - ' . date('d.m.Y H:i:s', $iTime) . PHP_EOL,
            FILE_APPEND
        );

        foreach ($arTasks as $arTask) {
            foreach ($arPortals as $obPortal) {
                ob_start();

                echo 'start ' . $arTask['ID'] . PHP_EOL;
                echo 'from ' . $arTask['PORTAL_ID'] . ($arTask['PORTAL_ID'] == 1 ? ' (box)' : ' (cloud)') . PHP_EOL;
                echo 'to ' . $obPortal->getId() . ($obPortal->getId() == 1 ? ' (box)' : ' (cloud)') . PHP_EOL;

                try {
                    if (!$obBoxPortal->taskExist($arTask)) {
                        $iNewBoxTaskId = $obBoxPortal->addTask($arTask);
                        echo 'new box_task_id - ' . $iNewBoxTaskId . PHP_EOL;
                    }

                    $iBoxTaskId = $obPortal->getBoxTaskId($arTask);

                    echo 'box_task_id - ' . $iBoxTaskId . PHP_EOL;

                    if ($obPortal->taskExist($arTask, $iBoxTaskId)) {
                        echo 'taskExist - Y' . PHP_EOL;
                        $iTaskId = $obPortal->updateTask($arTask, $iBoxTaskId);
                    } else {
                        echo 'taskExist - N' . PHP_EOL;
                        $iTaskId = $obPortal->addTask($arTask, $iBoxTaskId);
                    }

                    echo 'result task id - ' . $iTaskId . PHP_EOL;

                    if (!empty($arTask['FILES'])) {
                        foreach ($arTask['FILES'] as $arFile) {
                            if (!$obBoxPortal->fileExist($arFile))
                                $iBoxFileId = File::addTaskFile($obBoxPortal, $iBoxTaskId, $arFile);
                            else
                                $iBoxFileId = $obBoxPortal->getBoxFileId($arFile);

                            if ($iBoxFileId > 0  && !$obPortal->fileExist($arFile, $iBoxFileId)) {
                                File::addTaskFile($obPortal, $iTaskId, $arFile, $iBoxFileId);
                            }
                        }
                    }
                } catch (\Exception $e) {
                    echo 'Error: ' . $e->getMessage() . PHP_EOL;
                }

                echo 'end ' . $arTask['ID'] . ' - from ' . $arTask['PORTAL_ID'] . PHP_EOL;
                echo str_repeat('=', 50) . PHP_EOL;

                file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/synclogs/sync.log', ob_get_clean(), FILE_APPEND);
            }
        }

        return __METHOD__ . '(' . time() . ');';
    }

    public static function syncComments($iTime = 0)
    {
        $iTime = intval($iTime);

        if ($iTime <= 0)
            $iTime = time() - 60;

        $arPortals = PortalManager::getAll();

        if (!$arPortals)
            return __METHOD__ . '(' . time() . ');';

        $arComments = $arBoxTasksId = array();

        $rsSyncRows = TaskTable::getList();

        while ($arSyncRow = $rsSyncRows->fetch()) {
            $obPortal = PortalManager::getById($arSyncRow['PORTAL_ID']);

            try {
                $arPortalComments = $obPortal->getComments($arSyncRow['PORTAL_TASK_ID'], $iTime);
                $arComments = array_merge($arComments, $arPortalComments);
            } catch (\Exception $exception) {
            }

            $arBoxTasksId[$arSyncRow['TASK_ID']] = $arSyncRow['TASK_ID'];
        }

        foreach ($arBoxTasksId as $iTaskId) {
            $obBoxPortal = PortalManager::getBox();

            try {
                $arPortalComments = $obBoxPortal->getComments($iTaskId, $iTime);
                $arComments = array_merge($arComments, $arPortalComments);
            } catch (\Exception $exception) {
            }
        }

        file_put_contents(
            $_SERVER['DOCUMENT_ROOT'] . '/synclogs/comments.log',
            str_repeat('=', 20) . date('d.m.Y H:i:s') . str_repeat('=', 20) . PHP_EOL .
            print_r($arComments, true) . PHP_EOL,
            FILE_APPEND
        );

        foreach ($arComments as $arComment) {
            foreach ($arPortals as $obPortal) {
                $obPortal->addComment($arComment);
            }
        }

        return __METHOD__ . '(' . time() . ');';
    }

    public static function syncCheckLists($iTime = 0)
    {
        $iTime = intval($iTime);

        if ($iTime <= 0)
            $iTime = time() - 60;

        $arPortals = PortalManager::getAll();

        if (!$arPortals)
            return __METHOD__ . '(' . time() . ');';

        $arTasks = array();

        $obBoxPortal = PortalManager::getBox();

        foreach ($arPortals as $obPortal) {
            $arTasks = array_merge($arTasks, $obPortal->getTasks($iTime));
        }

        $arCheckLists = $arBoxTasksId = array();

        $rsSyncRows = TaskTable::getList();

        while ($arSyncRow = $rsSyncRows->fetch()) {
            $obPortal = PortalManager::getById($arSyncRow['PORTAL_ID']);
            try {
                $arPortalCheclists = $obPortal->getCheckLists($arSyncRow['PORTAL_TASK_ID']);

                $arCheckLists = array_merge($arCheckLists, $arPortalCheclists);
            } catch (\Exception $exception) {
            }

            $arBoxTasksId[$arSyncRow['TASK_ID']] = $arSyncRow['TASK_ID'];
        }

        foreach ($arBoxTasksId as $iTaskId) {
            $obBoxPortal = PortalManager::getBox();
            try {
                $arPortalCheclists = $obBoxPortal->getCheckLists($iTaskId);
                $arCheckLists = array_merge($arCheckLists, $arPortalCheclists);
            } catch (\Exception $exception) {
            }
        }

        foreach ($arCheckLists as $arCheckList) {
            foreach ($arPortals as $obPortal) {

                try {
                    if (!$obBoxPortal->checkListExist($arCheckList)) {
                        $iNewBoxCheckListId = $obBoxPortal->addCheckList($arCheckList);
                        echo 'new box_task_id - ' . $iNewBoxCheckListId . PHP_EOL;
                    }
                    $iBoxChecklistId = $obPortal->getBoxCheckListId($arCheckList);

                    echo 'box_task_id - ' . $iBoxChecklistId . PHP_EOL;

                    if ($obPortal->checkListExist($arCheckList, $iBoxChecklistId)) {
                        echo 'taskExist - Y' . PHP_EOL;
                        $iTaskId = $obPortal->updateCheckList($arCheckList, $iBoxChecklistId);
                    } else {
                        echo 'taskExist - N' . PHP_EOL;
                        $iTaskId = $obPortal->addCheckList($arCheckList, $iBoxChecklistId);
                    }

                    echo 'result task id - ' . $iTaskId . PHP_EOL;

                } catch (\Exception $exception) {
                }
            }
        }

        return __METHOD__ . '(' . time() . ');';
    }

}