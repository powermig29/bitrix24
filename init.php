<?php

/**
 * Устанавливает группу в задачу когда ее создает экстранет пользователь. Антон Долганин
 */
AddEventHandler('tasks', 'OnBeforeTaskAdd', 'OnBeforeTaskAddExtranet');
function OnBeforeTaskAddExtranet(&$fields)
{
    if (!$fields['GROUP_ID']) {
        if (
            Loader::includeModule('extranet') && CExtranet::IsExtranetSite() && CExtranet::IsExtranetUser()
        ) {
            $res = CSocNetGroup::GetList(array('DATE_ACTIVITY' => 'DESC'), array('CHECK_PERMISSIONS' => $GLOBALS['USER']->GetID(), 'SITE_ID' => SITE_ID));
            if ($row = $res->Fetch()) {
                $fields['GROUP_ID'] = $row['ID'];
            }
        }
    }
}

/**
 * Вызывается в момент удаления рабочей группы.
 * https://dev.1c-bitrix.ru/api_help/socialnetwork/events/OnSocNetGroupDelete.php
 */
AddEventHandler('socialnetwork', 'OnSocNetGroupDelete', 'OnSocNetGroupDeleteHandler');
function OnSocNetGroupDeleteHandler($ID) {
    if(Loader::IncludeModule('socialnetwork') && Loader::includeModule('crm')) {
        $arGroup = CSocNetGroup::GetList(array("ID" => "DESC"), array('ID' => $ID), false, false, array('UF_CRM_COMPANY'))->Fetch();
        if($arGroup["UF_CRM_COMPANY"]) {
            $entity = new CCrmCompany(false);
            $fields = array(
                'UF_GROUP_ID' => '',
                'UF_USE_EX_GROUP' => ''
            );
            $entity->update($arGroup["UF_CRM_COMPANY"], $fields);
        }
    }
}

/**
 * Событие "OnBeforeUserDelete" вызывается перед удалением пользователя.
 * Как правило задачи обработчика данного события - разрешить или запретить удаление пользователя.
 * https://dev.1c-bitrix.ru/api_help/main/events/onbeforeuserdelete.php
 */
AddEventHandler('main', 'OnBeforeUserDelete', 'OnBeforeUserDeleteHandler');
function OnBeforeUserDeleteHandler($user_id) {
    if(Loader::includeModule('crm')) {
        $order = array('sort' => 'asc');
        $sort = 'sort';
        $arUser = CUser::GetList($order, $sort, array("ID" => $user_id), array("NAV_PARAMS" => array('nTopCount' => 1), "SELECT" => array("UF_CRM_CONTACT_ID")))->Fetch();
        if ($arUser['UF_CRM_CONTACT_ID']) {
            $entity = new CCrmContact(false);
            $fields = array(
                'UF_SITE_USER' => '',
                'UF_USE_EXTRANET' => ''
            );
            $entity->update($arUser['UF_CRM_CONTACT_ID'], $fields);
        }
    }
}