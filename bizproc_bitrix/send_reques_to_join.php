<?php
/**
 * Отправка приглашения в группу внешенму пользователю из БП Битрикс.
 * Отправляет пользователю предложение присоединиться к рабочей группе. Метод статический.
 * https://dev.1c-bitrix.ru/api_help/socialnetwork/classes/csocnetusertogroup/SendRequestToJoinGroup.php
 */

CModule::IncludeModule("socialnetwork");

$groupId = intval($this->GetVariable("WORKGROUP_ID"));
$ownerId = intval($GLOBALS["USER"]->GetID());
$userId = intval($this->GetVariable("USER_ID"));
$sendEmail = false;

CSocNetUserToGroup::SendRequestToJoinGroup($ownerId, $userId, $groupId, "", $sendEmail);