<?php
/**
 * Установка ID задачи в элемент списка и в пользовательское свойство задачи
 * Метод сохраняет значения всех свойств элемента информационного блока
 * https://dev.1c-bitrix.ru/api_help/iblock/classes/ciblockelement/setpropertyvaluesex.php
 */

CModule::IncludeModule("iblock");
CModule::IncludeModule("tasks");

global $USER;

$elementId = intval("{=A92826_12728_45926_30804:ElementId}");
$taskId = intval("{=A42938_6207_77067_27645:TaskId}");
CIBlockElement::SetPropertyValuesEx($elementId, false, array("TASK_ID" => $taskId));

$oTaskItem = new CTaskItem($taskId, $USER->GetID());
$rs = $oTaskItem->Update(
    array(
        "UF_CRM_LIST_ELEMENTS" => array($elementId)
    )
);