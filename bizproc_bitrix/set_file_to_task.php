<?php
/**
 * Запись файлов в задачу из Бизнес процесса Битрикс (коробка)
 * Нестатический метод изменяет параметры задачи с идентификатором ID.
 * https://dev.1c-bitrix.ru/api_help/tasks/classes/ctaskitem/update.php
 */

if (CModule::IncludeModule("tasks")) {
    global $USER;

    $TaskID = intval("{=A35624_6974_66296_9115:TaskId}");
    $arFiles = array();
    $fileId_1 = intval("{=A4689_55361_44816_78762:ObjectId}");
    $fileId_2 = intval("{=A65582_20982_20417_50466:ObjectId}");

    if($fileId_1 > 0) {
        $arFiles[] = "n" . $fileId_1;
    }
    if($fileId_2 > 0) {
        $arFiles[] = "n" . $fileId_2;
    }

    $oTaskItem = new CTaskItem($TaskID, $USER->GetID());
    $rs = $oTaskItem->Update(
        array(
            "UF_TASK_WEBDAV_FILES" => $arFiles
        ),
        array(
            "USER_ID" => $USER->GetID(),
            "CHECK_RIGHTS_ON_FILES" => false
        )
    );
}