<?php
/**
 * Получение ID группы из компании
 */

CModule::IncludeModule("crm");
$companyId = "{=Document:COMPANY_ID}";

$arCompany = CCrmCompany::GetListEx(array(), array("ID" => $companyId), false, array("nTopCount" => 1), array("UF_GROUP_ID"))->Fetch();
$this->SetVariable("WORKGROUP_ID", $arCompany["UF_GROUP_ID"]);