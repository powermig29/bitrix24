<?php
/**
 * Создание экстранет группы из Бизнес процесса Битрикс.
 * Статический метод создает новую рабочую группу.
 * https://dev.1c-bitrix.ru/api_help/socialnetwork/classes/CSocNetGroup/CreateGroup.php
 */

CModule::IncludeModule("socialnetwork");
CModule::IncludeModule("disk");
CModule::IncludeModule("webdav");
CModule::IncludeModule("extranet");

$SocGroup = new CSocNetGroup;

$arFieldsSG = array(
    "NAME" => "{=Document:TITLE}",
    "SITE_ID" => Array(0 => SITE_ID, 1 => CExtranet::GetExtranetSiteID()),
    "DESCRIPTION" => "{=Document:TITLE} внешняя группа",
    "ACTIVE" => "Y",
    "VISIBLE" => "N",
    "OPENED" => "N",
    "CLOSED" => "N",
    "SUBJECT_ID" => 1,
    "OWNER_ID" => $GLOBALS["USER"]->GetID(),
    "INITIATE_PERMS" => "E",
    "SPAM_PERMS" => "K",
    "UF_CRM_COMPANY" => "{=Document:ID}" // CRM company
);

$GroupID = $SocGroup->CreateGroup($USER->GetID(), $arFieldsSG);

if ($GroupID > 0) {
    \Bitrix\Disk\Driver::getInstance()->addGroupStorage($GroupID);
}

$this->SetVariable("WORKGROUP_ID", $GroupID);