<?php
/**
 * Регистрация экстранет пользователя из БП Битрикс
 */

CModule::IncludeModule("intranet");

$userData["EMAIL"] = "{=Document:EMAIL_PRINTABLE}";
$userData["LOGIN"] = "{=Document:EMAIL_PRINTABLE}";
$userData["CONFIRM_CODE"] = randString(8);
$inviteText = htmlspecialcharsbx("{=Constant:INVITE_TEXT}");

$ID = CIntranetInviteDialog::RegisterUser($userData, SITE_ID);

if(is_array($ID))
{
    foreach ($ID as $strErrorTmp)
    {
        AddMessage2Log($strErrorTmp);
    }
}
else
{
    $oUser = new CUser();
    $arGroups = CIntranetInviteDialog::getUserGroups(SITE_ID, true);
    $oUser->Update($ID, Array(
            "GROUP_ID" => $arGroups,
            "UF_CRM_CONTACT_ID" => "{=Document:ID}"
        )
    );
    $userData['ID'] = $ID;
    CIntranetInviteDialog::InviteUser($userData, $inviteText);
    $this->SetVariable("USER_ID", $ID);
}