<?php
/**
 * Получение пользователей группы из БП Битрикс (коробка)
 */

CModule::IncludeModule("socialnetwork");
CModule::IncludeModule("extranet");

$intranetUsers = array();
$extranetUsers = array();
$groupID =  $this->GetVariable("GROUP_ID");

$dbRequests = CSocNetUserToGroup::GetList(
    array("USER_LAST_NAME" => "ASC", "USER_NAME" => "ASC"),
    array("GROUP_ID" => $groupID, "USER_ACTIVE" => "Y"),
    false,
    false,
    array("USER_ID")
);

while ($arUser = $dbRequests->GetNext())
{
    if (in_array(CExtranet::GetExtranetUserGroupID(), CUser::GetUserGroup($arUser["USER_ID"])))
    {
        $extranetUsers[] = "user_" .  $arUser["USER_ID"];
    } else {
        $intranetUsers[] = "user_" .  $arUser["USER_ID"];
    }
}

if(count($extranetUsers)) {
    $this->SetVariable("EXTRANET_USER", $extranetUsers);
}

if(count($intranetUsers)) {
    $this->SetVariable("GROUP_USER", $intranetUsers);
}