<?php

/**
 * Добавление пользователей в группу из БП Битрикс (коробка)
 * Метод добавляет новую связь между пользователем и группой. Метод нестатический.
 * https://dev.1c-bitrix.ru/api_help/socialnetwork/classes/csocnetusertogroup/Add.php
 */

CModule::IncludeModule("socialnetwork");
$groupId = $this->GetVariable("WORKGROUP_ID");
$ownerId = $GLOBALS["USER"]->GetID();
$strUsers = "{=Constant:GROUP_USERS}";
$inviteText = htmlspecialcharsbx("{=Constant:INVITE_TEXT}");

$users = str_replace(" ", "", $strUsers);
$arUsers = explode(",", $users);
$l = strlen("user_");

foreach ($arUsers as $user) {
    if (substr($user, 0, $l) === "user_") {
        $user = intval(substr($user, $l));
        if ($user > 0 && $user != $ownerId) {
            $result = CSocNetUserToGroup::Add(array(
                    "USER_ID" => $user,
                    "GROUP_ID" => $groupId,
                    "ROLE" => SONET_ROLES_USER,
                    "=DATE_CREATE" => $GLOBALS["DB"]->CurrentTimeFunction(),
                    "=DATE_UPDATE" => $GLOBALS["DB"]->CurrentTimeFunction(),
                    "INITIATED_BY_TYPE" => SONET_INITIATED_BY_GROUP,
                    "INITIATED_BY_USER_ID" => $ownerId,
                    "MESSAGE" => (strlen($inviteText) ? $inviteText : false),
                )
            );
        }
    }
}