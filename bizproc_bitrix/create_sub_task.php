<?php
/**
 * Создать подзадачу из БП Битрикс
 */

if (CModule::IncludeModule("tasks")) {
    global $USER;

    $arFields = Array(
        "TITLE" => "Task title",
        "DESCRIPTION" => "Task description",
        "RESPONSIBLE_ID" => $USER->GetID(),
        "GROUP_ID" => $this->GetVariable('WORKGROUPID'),
        "PARENT_ID" => '{=A2629_10558_53461_99205:TaskId}',
    );

    $obTask = new CTasks;
    $ID = $obTask->Add($arFields);
    $success = ($ID > 0);
}